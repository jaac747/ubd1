DROP TABLE CLIENTS;
DROP TABLE CORE_MODULES_DETAIL;
DROP TABLE CORE_MODULES;
DROP TABLE CURRENCIES;
DROP TABLE USERS_ROLES;
DROP TABLE USERS_PROFILES;
DROP TABLE USERS_PROFILES_DETAIL;
DROP TABLE USERS;
DROP TABLE PASSWORD_RESETS;
DROP TABLE SALES_PRODUCTS_DETAIL;
DROP TABLE SALE_CATEGORIES;
DROP TABLE SALES_PRODUCTS;
DROP TABLE ESTIMATES;
DROP TABLE ESTIMATES_PRODUCTS_DETAIL;
DROP TABLE PROVIDERS;

DROP SEQUENCE SEQUENCE_CATEGORIES;
DROP SEQUENCE SEQUENCE_CLIENTS;
DROP SEQUENCE SEQUENCE_CURRENCIES;
DROP SEQUENCE SEQUENCE_ESTIMATES;
DROP SEQUENCE SEQUENCE_ESTIMATES_DETAIL;
DROP SEQUENCE SEQUENCE_MODULES;
DROP SEQUENCE SEQUENCE_MODULES_DETAIL;
DROP SEQUENCE SEQUENCE_PRODUCTS;
DROP SEQUENCE SEQUENCE_PRODUCT_DETAIL;
DROP SEQUENCE SEQUENCE_USER;
DROP SEQUENCE SEQUENCE_USER_PROF;
DROP SEQUENCE SEQUENCE_USER_PROF_DETAIL;
DROP SEQUENCE SEQUENCE_USER_ROLE;
DROP SEQUENCE SEQUENCE_PROVIDERS;

SELECT * FROM SALE_CATEGORIES;

CREATE TABLE CLIENTS(
ID              NUMBER(20),
NAME_CLIENTE    VARCHAR2(200) NOT NULL,
IMAGE           VARCHAR2(200),
PHONE           NUMBER(11),
MAIL            VARCHAR2(250),
ADDRESS         VARCHAR2(250),
CODE            VARCHAR2(200) NOT NULL,
ACTIVE          VARCHAR2(5) DEFAULT 'Y' NOT NULL,
CREATED_AT       TIMESTAMP,
UPDATED_AT       TIMESTAMP,
CONSTRAINT CLIENT_PK_ID PRIMARY KEY (ID),
CONSTRAINT CLIENT_ACTIVE_VALUE CHECK (ACTIVE IN ('Y','N')),
CONSTRAINT CLIENTS_CODE_UNIQUE UNIQUE (CODE)
);

CREATE SEQUENCE SEQUENCE_CLIENTS START WITH 1 INCREMENT BY 1;



CREATE TABLE CORE_MODULES(
ID              NUMBER(20),
NAME_MODULE     VARCHAR2(200) NOT NULL,
QUICK_NAME_MODULE   VARCHAR2(200) NOT NULL,
ONLY_WEBMASTER  VARCHAR2(5)  DEFAULT 'N' NOT NULL,
CREATED_AT       TIMESTAMP,
UPDATED_AT       TIMESTAMP,
CONSTRAINT MODULE_PK_ID PRIMARY KEY (ID),
CONSTRAINT MODULE_WEBMASTER_VALUE CHECK (ONLY_WEBMASTER IN ('Y','N')),
CONSTRAINT MODULES_QUICK_NAME_UNIQUE UNIQUE(QUICK_NAME_MODULE),
CONSTRAINT MODULES_NAME_UNIQUE UNIQUE(NAME_MODULE)
);

CREATE SEQUENCE SEQUENCE_MODULES START WITH 1 INCREMENT BY 1;


CREATE TABLE CORE_MODULES_DETAIL(
ID              NUMBER(20),
ID_MODULE       NUMBER(20),
NAME_WINDOW     VARCHAR2(200) NOT NULL,
QUICK_NAME_WINDOW   VARCHAR2(200) NOT NULL,
ROUTE           VARCHAR2(200) NOT NULL,
DESCRIPTION_NAME_WINDOW VARCHAR2(512) NOT NULL,
CREATED_AT       TIMESTAMP,
UPDATED_AT       TIMESTAMP,
CONSTRAINT WINDOW_PK_ID PRIMARY KEY (ID),
CONSTRAINT WINDOW_QUICK_NAME_UNIQUE UNIQUE(QUICK_NAME_WINDOW),
CONSTRAINT FK_CORE_MODULE FOREIGN KEY (ID_MODULE) REFERENCES CORE_MODULES (ID)
);

CREATE SEQUENCE SEQUENCE_MODULES_DETAIL START WITH 1 INCREMENT BY 1;


CREATE TABLE CURRENCIES(
ID              NUMBER(19), 
NAME_CURRENCY   VARCHAR2(200) NOT NULL,
ICON            VARCHAR2(200) NOT NULL,
FEE             NUMBER(6,2) NOT NULL,
URL_API         VARCHAR2(200),
ACTIVE          VARCHAR2(5) DEFAULT 'Y' NOT NULL,
CREATED_AT       TIMESTAMP,
UPDATED_AT       TIMESTAMP,
CONSTRAINT CURRENCIES_PK_ID PRIMARY KEY (ID),
CONSTRAINT CURRENCIES_ACTIVE_VALUE CHECK (ACTIVE IN ('Y','N')),
CONSTRAINT CURRENCIES_NAME_UNIQUE UNIQUE(NAME_CURRENCY)
);

CREATE SEQUENCE SEQUENCE_CURRENCIES START WITH 1 INCREMENT BY 1;



CREATE TABLE SALES_PRODUCTS(
ID                  NUMBER(19), 
NAME_PRODUCT        VARCHAR2(200),    
DESCRIPTION_PRODUCT VARCHAR2(500),
ACTIVE              VARCHAR2(5) DEFAULT 'Y' NOT NULL,
IMAGE_PRODUCT       VARCHAR2(200),
ID_CATEGORY         NUMBER(19),
TYPE_PRODUCT        VARCHAR2(20) DEFAULT 'Simple' NOT NULL,
COLUMN_UNION        VARCHAR2(200),
CURRENCY            VARCHAR2(200),
MIN_EXIST           NUMBER(11),
ID_PROVIDER         NUMBER(20),
CREATED_AT          TIMESTAMP,
UPDATED_AT          TIMESTAMP,
ICON                VARCHAR2(200),
CONSTRAINT PRODUCT_PK_ID PRIMARY KEY (ID),
CONSTRAINT PRODUCT_STATUS CHECK (TYPE_PRODUCT IN ('Simple','Union'))
);

CREATE SEQUENCE SEQUENCE_PRODUCTS START WITH 1 INCREMENT BY 1;


CREATE TABLE SALE_CATEGORIES(
ID              NUMBER(19), 
NAME_CATEGORY   VARCHAR2(200) NOT NULL,
QUICK_NAME_CATEGORY  VARCHAR2(200),
IMAGE_CATEGORY  VARCHAR2(200),
ACTIVE          VARCHAR2(5) DEFAULT 'Y' NOT NULL,
CREATED_AT       TIMESTAMP,
UPDATED_AT       TIMESTAMP,
ICON            VARCHAR2(200),
CONSTRAINT CATEGORIES_PK_ID PRIMARY KEY (ID),
CONSTRAINT CATEGORIES_ACTIVE CHECK (ACTIVE IN ('Y','N'))
);

CREATE SEQUENCE SEQUENCE_CATEGORIES START WITH 1 INCREMENT BY 1;

CREATE TABLE sales_products_detail (
  id 			NUMBER(20)  NOT NULL ,
  id_product 	NUMBER(19) ,
  name_product_detail VARCHAR2(191) ,
  type_product_detail VARCHAR2(15)   DEFAULT 'unity' NOT NULL,
  price_unity NUMBER(6,2) ,
  price_weight NUMBER(6,2) ,
  price_before_update NUMBER(19) ,
  active varchar2(15)  DEFAULT 'Y' NOT NULL,
  description VARCHAR2(191)  ,
  offer VARCHAR2(191)  ,
  time_limit VARCHAR2(15) DEFAULT '3' NOT NULL,
  date_of_expiry timestamp ,
  quantity_exist_unity NUMBER(19) ,
  quantity_reservation_unity NUMBER(19) ,
  quantity_exist_weight NUMBER(19) ,
  quantity_reservation_weight NUMBER(19) ,
  code_inventory varchar2(191)  ,
  created_at timestamp  ,
  updated_at timestamp  ,
  icon varchar2(191)  ,
  image varchar2(191)  ,
  ID_PROVIDER         NUMBER(20),
 CONSTRAINT PROD_DET_PK_ID PRIMARY KEY (ID),
CONSTRAINT PROD_DET_TYPE CHECK (type_product_detail IN ('unity','weight')),
CONSTRAINT PROD_DET_LIMIT CHECK (time_limit IN ('3','6','9','12','18','24')),
CONSTRAINT PROD_DET_ACTIVE CHECK (ACTIVE IN ('Y','N')),
CONSTRAINT FK_PROD_DET_CATEGORYES FOREIGN KEY (id_product) REFERENCES SALES_PRODUCTS (ID)
) ;


CREATE SEQUENCE SEQUENCE_PRODUCT_DETAIL START WITH 1 INCREMENT BY 1;

CREATE TABLE users (
  id NUMBER(20),
  name VARCHAR2(191)  NOT NULL,
  email VARCHAR2(191)  NOT NULL,
  email_verified_at timestamp  ,
  password VARCHAR2(191)  NOT NULL,
  un_password VARCHAR2(191)  ,
  id_user_profile NUMBER(19) ,
  id_role_access NUMBER(19) ,
  id_father_role NUMBER(19) ,
  id_child_role NUMBER(19) ,
  id_father_school NUMBER(19) ,
  id_child_school NUMBER(19) ,
  remember_token VARCHAR2(100)  ,
  created_at timestamp  ,
  updated_at timestamp  ,
  CONSTRAINT USER_PK_ID PRIMARY KEY (ID),
  CONSTRAINT users_email_unique UNIQUE(email)
) ;

CREATE SEQUENCE SEQUENCE_USER START WITH 1 INCREMENT BY 1;


CREATE TABLE users_profiles (
  id NUMBER(20) ,
  name_profile VARCHAR2(191)  NOT NULL,
  description_profile varchar2(512)  ,
  created_at timestamp  ,
  updated_at timestamp  ,
  CONSTRAINT USER_PROFILE_PK_ID PRIMARY KEY (ID),
  CONSTRAINT users_PROFILE_unique UNIQUE(name_profile)
) ;

CREATE SEQUENCE SEQUENCE_USER_PROF START WITH 1 INCREMENT BY 1;


CREATE TABLE users_profiles_detail (
  id NUMBER(20) ,
  id_user_profile NUMBER(19) NOT NULL,
  id_modules_detail NUMBER(19) NOT NULL,
  created_at timestamp NULL ,
  updated_at timestamp NULL ,
  CONSTRAINT USER_PROFILE_DET_PK_ID PRIMARY KEY (ID)
) ;

CREATE SEQUENCE SEQUENCE_USER_PROF_DETAIL START WITH 1 INCREMENT BY 1;


CREATE TABLE users_roles (
  id NUMBER(20) ,
  name_role VARCHAR2(191)  NOT NULL,
  quick_name_role VARCHAR2(191)  NOT NULL,
  id_branch NUMBER(19) ,
  color_role VARCHAR2(191)  ,
  id_child_role NUMBER(19) ,
  created_at timestamp NULL ,
  updated_at timestamp NULL ,
  CONSTRAINT USER_ROLE_PK_ID PRIMARY KEY (ID)
) ;
CREATE SEQUENCE SEQUENCE_USER_ROLE START WITH 1 INCREMENT BY 1;

CREATE TABLE ESTIMATES(
ID              NUMBER(19), 
ID_USER         NUMBER(19),
CORRELATIVE     NUMBER(19),
OBJESTIMATE     VARCHAR2(500),
TOTAL_ESTIMATE  NUMBER(6,2),
CLIENT_ID       NUMBER(19),
CLIENT_NAME     VARCHAR2(200),
CLIENT_EMAIL    VARCHAR2(200),
CLIENT_ADDRESS  VARCHAR2(200),
CLIENT_PHONE    VARCHAR2(200),
STATUS          VARCHAR2(20) DEFAULT 'Solicitado' NOT NULL,
CREATED_AT       TIMESTAMP,
UPDATED_AT       TIMESTAMP,
CONSTRAINT ESTIMATES_PK_ID PRIMARY KEY (ID),
CONSTRAINT ESTIMATES_STATUS_VALUE CHECK (STATUS IN ('Solicitado','Enviado','Entregado','Pagado'))
);
CREATE SEQUENCE SEQUENCE_ESTIMATES START WITH 1 INCREMENT BY 1;

CREATE TABLE ESTIMATES_PRODUCTS_DETAIL(
ID              NUMBER(19), 
ID_ESTIMATE     NUMBER(19),
ID_PRODUCT      NUMBER(19),
QUANTITY        NUMBER(6,2),
CATEGORY        NUMBER(11),
PRICE           NUMBER(6,2),
TOTAL_INDIV_PRODUCT_ESTIMATE           NUMBER(6,2),
CREATED_AT       TIMESTAMP,
UPDATED_AT       TIMESTAMP,
CONSTRAINT ESTIMATE_DETAIL_PK_ID PRIMARY KEY (ID),
CONSTRAINT FK_ESTIMATE_DET_ESTI FOREIGN KEY (ID_ESTIMATE) REFERENCES ESTIMATES (ID),
CONSTRAINT FK_ESTIMATE_DET_PROD FOREIGN KEY (ID_PRODUCT) REFERENCES SALES_PRODUCTS (ID)
);

CREATE SEQUENCE SEQUENCE_ESTIMATES_DETAIL START WITH 1 INCREMENT BY 1;

CREATE TABLE password_resets (
  email varchar(191)  NOT NULL,
  token varchar(191)  NOT NULL,
  created_at timestamp 
) ;


CREATE TABLE providers (
  id number(20) ,
  name_provider varchar2(191) NOT NULL,
  address varchar(191)  NOT NULL,
  phone varchar(191)  ,
  phone_two varchar(191)  ,
  link_share varchar2(15) DEFAULT 'N'  NOT NULL ,
  link_share_two varchar2(15) DEFAULT 'N'  NOT NULL ,
  email varchar(191)  ,
  created_at timestamp ,
  updated_at timestamp ,
  datei date,
  datef date,
  CONSTRAINT PROVIDERS_PK_ID PRIMARY KEY (ID),
  CONSTRAINT PROVIDERS_SHARE CHECK (link_share IN ('Y','N')),
  CONSTRAINT PROVIDERS_SHARE_TWO CHECK (link_share_two IN ('Y','N')),
  CONSTRAINT providers_address_unique UNIQUE(address)
);

CREATE SEQUENCE SEQUENCE_PROVIDERS START WITH 1 INCREMENT BY 1;


INSERT INTO users
(id, name, email, email_verified_at, password, un_password, id_user_profile, id_role_access, id_father_role, id_child_role, id_father_school, id_child_school, remember_token, created_at, updated_at)
VALUES(1, 'Webmaster', 'najcheu@gmail.com', NULL, '$2y$10$v2vsuQXLjbHMOamWw0ZzSefb/1eeTlAQXz7qoKZLTkuTa8ursMimS', '', 1, 1, NULL, NULL, NULL, NULL, 'jlz9eao7VLbEjIWeykvb4q3CUHfJyKstrrCEFmwNRTbNNYzEcv5NAiwIfJwM', NULL, NULL);
commit;


INSERT INTO core_modules
(id, name_module, quick_name_module, only_webmaster, created_at, updated_at)
VALUES(3, 'Configuracion', 'configuracion', 'Y', sysdate, sysdate);
INSERT INTO core_modules
(id, name_module, quick_name_module, only_webmaster, created_at, updated_at)
VALUES(4, 'Productos', 'productos', 'N', sysdate, sysdate);
INSERT INTO core_modules
(id, name_module, quick_name_module, only_webmaster, created_at, updated_at)
VALUES(5, 'Clientes', 'clientes', 'N', sysdate, sysdate);

commit;

INSERT INTO core_modules_detail
(id, id_module, name_window, quick_name_window, route, description_name_window, created_at, updated_at)
VALUES(2, 3, 'Configuracion de Ventanas', 'configuracion_de_ventanas', 'configuration', 'configuration', sysdate, sysdate);
INSERT INTO core_modules_detail
(id, id_module, name_window, quick_name_window, route, description_name_window, created_at, updated_at)
VALUES(3, 4, 'Categorias Productos', 'categorias_productos', 'categories', 'Categorias de los productos', sysdate, sysdate);
INSERT INTO core_modules_detail
(id, id_module, name_window, quick_name_window, route, description_name_window, created_at, updated_at)
VALUES(4, 4, 'Administracion productos', 'administracion_productos', 'admin-products', 'Administracion de productos', sysdate, sysdate);
INSERT INTO core_modules_detail
(id, id_module, name_window, quick_name_window, route, description_name_window, created_at, updated_at)
VALUES(5, 4, 'Monedas', 'monedas', 'currencies', 'Administracion de moneda utilizada', sysdate, sysdate);
INSERT INTO core_modules_detail
(id, id_module, name_window, quick_name_window, route, description_name_window, created_at, updated_at)
VALUES(6, 4, 'Temporadas', 'temporadas', 'temporadas', 'Administracion de temporadas', sysdate, sysdate);
INSERT INTO core_modules_detail
(id, id_module, name_window, quick_name_window, route, description_name_window, created_at, updated_at)
VALUES(7, 5, 'Clientes', 'clientes', 'clients', 'Administracion de clientes', sysdate,sysdate);
commit;


--Crear trigger para users y estimates--

CREATE TRIGGER trig_sequence_user
  BEFORE INSERT ON users
  FOR EACH ROW
  BEGIN
    SELECT sequence_user.nextval INTO :new.id FROM dual;
  END
;

CREATE TRIGGER trig_sequence_estimates
  BEFORE INSERT ON estimates
  FOR EACH ROW
  BEGIN
    SELECT SEQUENCE_ESTIMATES.nextval INTO :new.id FROM dual;
  END
;

--Crear trigger sales products--

CREATE TRIGGER trig_sequence_product
  BEFORE INSERT ON sales_products
  FOR EACH ROW
  BEGIN
    SELECT SEQUENCE_PRODUCTS.nextval INTO :new.id FROM dual;
  END
;

--Crear trigger para sales_product_detail--

CREATE TRIGGER trig_sequence_product_detail
  BEFORE INSERT ON sales_products_detail
  FOR EACH ROW
  BEGIN
    SELECT SEQUENCE_PRODUCT_DETAIL.nextval INTO :new.id FROM dual;
  END
;

--Crear trigger categorias--

CREATE TRIGGER trig_sequence_categories
  BEFORE INSERT ON sale_categories
  FOR EACH ROW
  BEGIN
    SELECT SEQUENCE_CATEGORIES.nextval INTO :new.id FROM dual;
  END
;


--eliminar llave foranea estimates:--

alter table estimates
drop CONSTRAINT FK_ESTIMATES_USER;  

--Alter table estimates_products_detail--

alter table estimates_products_detail
rename COLUMN TOTAL_INDIV_PRODUCT_ESTIMATE to TOTAL_INDIVIDUAL_PRODUCT_ESTIMATE;

--Delete Constraint estimates_products_detail--

alter table estimates_products_detail
drop CONSTRAINT FK_ESTIMATE_DET_PROD;


--Inserts Categories--

insert into sale_categories(name_category,active) values('Navidad','Y');

insert into sale_categories(name_category,active) values('Ejercito','Y');

insert into sale_categories(name_category,active) values('Halloween','Y');

insert into sale_categories(name_category,active) values('San Valentin','Y');

insert into sale_categories(name_category,active) values('Herramientas','Y');

insert into sale_categories(name_category,active) values('Baño','Y');

insert into sale_categories(name_category,active) values('Jardin','Y');