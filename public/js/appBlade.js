const objTabsConfig = {
    modules : {
        container: 'cntModules',
        strDescription: 'Modules',
        functionExecute: 'drawAdminModules',
        pivot: 'Y',
        disabled: false,
    },
    detailModules : {
        container: 'cntModulesDetail',
        strDescription: 'Modules Detail',
        functionExecute: '',
        pivot: 'N',
        disabled: false,
    },
    a : {
        container: 'a',
        strDescription: 'Modules Detail',
        functionExecute: '',
        pivot: 'N',
        disabled: true,
    },
};
/*const tabs = new drawTabs({cntTabs: $(`#cntTabs`), objTabs: objTabsConfig});
tabs.draw();*/
