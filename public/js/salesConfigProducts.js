document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, options);
});

// Or with jQuery

$(document).ready(function(){
    $('.modal').modal();

    var instance = M.Modal.getInstance(elem);
    instance.open();
    instance.close();
    instance.destroy();
});
