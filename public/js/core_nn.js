function d (myVar = null, strDescription = '') {
    let v = myVar;
    const varType = typeof(myVar);
    console.log(`\r\r`);
    let strDebug = `~~~~~NN-Debug~~~~~\n
Variable ~ ${strDescription} ~ is type ${varType}`;
    console.log(strDebug);
    console.log(v);
    return console.log(`==========================================================================================================================================================================================================================================================`);
}

function S () {
    const Constructor = function (selector) {
        if (!selector) return;
        if (selector === 'document') {
            this.elems = [document];
        }
        else if (selector === 'window') {
            this.elems = [window];
        }
        else {
            this.elems = document.querySelectorAll(selector);
        }
    };

    let instantiate = function (selector) {
        return new Constructor(selector);
    };

    /*Wait all my heart not use me*/
    Constructor.prototype.each = function (callback) {
        if (!callback || typeof callback !== 'function') return;
        for (let i = 0; i < this.elems.length; i++) {
            callback(this.elems[i], i);
        }
    };

    Constructor.prototype.addClass = function (className) {
        this.each(function (item) {
            item.classList.add(className);
        });
    };

    Constructor.prototype.removeClass = function (className) {
        this.each(function (item) {
            item.classList.remove(className);
        });
    };

    return instantiate;
}

function drawTabs (objSettings) {
    const self = this;
    const defaults = {
        cntTabs: undefined,
        objTabs: {}
    };

    /*const objExample = {
        example: {
            container: 'id',
            strDescription: 'Example',
            functionExecute: functionExecute,
            pivot: 'Y',
            disabled: false,
            params : {},
        },
    };

    function functionExecute(){};*/

    objSettings || (objSettings = {});
    let settings = Object.assign({}, defaults, objSettings);

    this.init = () => {
        if (typeof settings.cntTabs !== "undefined" && Object.keys(settings.objTabs).length > 0) {
            self.clearAllContents();
            self.draw();
        }
        else {
            d("no tiene completos los parámetros, vea la función para verificar.")
        }
    };

    this.clearAllContents = () => {
        for(let key in settings.objTabs) {
            const val = settings.objTabs[key];
            $(`#cnt-d-${val.container}`).html('');
        }
    };

    this.draw = () => {
        const element = `   <ul class="nav nav-tabs" id="nnTabs" role="tablist"></ul>
                            <div class="tab-content" id="nnContentTabs"></div>`;
        $(settings.cntTabs).append(element);
        const cntTitle = $(`#nnTabs`);
        const cntDetail = $(`#nnContentTabs`);
        for(let key in settings.objTabs) {
            const val = settings.objTabs[key];
            let boolActive = (val.pivot.toUpperCase() === 'Y') ? 'active' : '';
            let boolDisabled = (boolActive  === 'active') ? 'show active' : '';
            let li = `  <li class="nav-item">
                            <a class="nav-link ${boolActive}" id="cnt-d-${val.container}-tab" data-toggle="tab" href="#cnt-d-${val.container}" role="tab" aria-controls="home" aria-selected="true" style="padding: 7px 12px !important;">
                                ${val.strDescription}
                            </a>
                        </li>`;
            $(cntTitle).append(li);

            const detail = `<div class="tab-pane fade ${boolDisabled}" id="cnt-d-${val.container}" role="tabpanel" aria-labelledby="home-tab"></div>`;
            $(cntDetail).append(detail);

            const callback = val.functionExecute;
            if(callback) {
                $(`#cnt-d-${val.container}-tab`).on('click', () => {
                    if(Object.keys(val.params).length > 0) {
                        self.fCallback(callback, val.params);
                    }
                    else {
                        self.fCallback(callback);
                    }
                });
            }
            if(boolActive === 'active') {
                $(`#cnt-d-${val.container}-tab`).click();
            }
        }
    };

    this.fCallback = (callback, objParams = {}) => {
        if(Object.keys(objParams).length > 0){
            //no terminado por que aún no lo necesito
            /*for(let key in objParams){
                let val = objParams[key];
            }*/
        }
        else {
            callback();
        }
    };
}

function drawModal (objSettings) {
    const self = this;
    const defaults = {
        idModal: 'nnModal',
        title: 'Mensaje del sistema',
        content: '<h1>Información</h1>',
        boolClose: true,
        objButtons: {},
    };
    objSettings || (objSettings = {});
    let settings = Object.assign({}, defaults, objSettings);

    this.showModal = () => {
        self.create();
        setTimeout( () => {
            $(`#${settings.idModal}`).modal('show')
        }, 100 );
    };

    this.create = () => {
        let elementsButton = `<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>`;
        if(!settings.boolClose) {
            elementsButton = '';
        }
        if(Object.keys(settings.objButtons).length > 0) {
            for(let key in settings.objButtons){
                const val = settings.objButtons[key];
                let i = '';
                if(val.icon !== '') {
                    i = `<i class="fa ${val.icon}"></i>`;
                }
                elementsButton += ` <button id="${val.id}" class="btn ${val.classButton}" type="button" onclick="${val.functionExecute}()">
                                        ${val.description}
                                        ${i}
                                    </button>`;
            }
        }
        let element = ` <div id="${settings.idModal}" class="modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        ${settings.title}
                                    </div>
                                    <div class="modal-body">
                                        ${settings.content}
                                    </div>
                                    <div class="modal-footer">
                                        ${elementsButton}
                                    </div>
                                </div>
                            </div>                        
                        </div>`;
        $(`#cntAll`).append(element);
    };
}
