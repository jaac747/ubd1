drawProductsInCart();

function showCart()
{
    let element = document.getElementById("cntAllProducts");
    let intTotal = getCount(element, false);
    if((intTotal * 1) > 0) {
        $(".cnt-cart").addClass("cnt-cart-show");
        drawCountCart(intTotal);
    }
    else {
        let objCart = JSON.parse(localStorage.getItem('cart'));
        if(objCart !== null && Object.keys(objCart).length > 0) {
            $(".cnt-cart").addClass("cnt-cart-show");
            drawProductsInCart();
        }
    }
}

function getCount(parent, getChildrensChildren)
{
    let relevantChildren = 0;
    let children = parent.childNodes.length;
    for(let i=0; i < children; i++){
        if(parent.childNodes[i].nodeType != 3){
            if(getChildrensChildren)
                relevantChildren += getCount(parent.childNodes[i],true);
            relevantChildren++;
        }
    }
    return relevantChildren;
}

function closeCart()
{
    $(".cnt-cart").removeClass("cnt-cart-show");
}

function setObjCart(intProduct)
{
    if(typeof intProduct !== 'undefined') {
        let objCart = JSON.parse(localStorage.getItem('cart'));
        if(objCart === null) {
            objCart = {};
        }
        if(typeof objCart[intProduct] !== 'undefined') {
            objCart[intProduct]['quantity'] = $(`#product_${intProduct}`).val();
        }
        else {
            objCart[intProduct] = {
                'quantity': $(`#product_${intProduct}`).val(),
                'info': {
                    'price': $(`#price_${intProduct}`).val(),
                    'name': $(`#name_${intProduct}`).val(),
                    'category': $(`#strCategory_${intProduct}`).val(),
                    'id_category': $(`#id_category_${intProduct}`).val(),
                },
            };
        }

        let strCart = JSON.stringify(objCart);
        localStorage.setItem('cart', strCart);
        drawProductsInCart();
    }
}

function drawProductsInCart()
{
    let objCart = JSON.parse(localStorage.getItem('cart'));
    if(typeof objCart !== 'undefined' && Object.keys(objCart).length > 0) {
        $(`#cntAllProducts`).html('');
        let strStyle = 'd9d9d9';
        for(let key in objCart){
            let val = objCart[key];
            let total = (val.quantity * 1) * (val.info.price);
            strStyle = (strStyle === 'd9d9d9') ? 'ffffff' : 'd9d9d9';
            $(`#product_${key}`).val(val.quantity);
            $(`#badge_${key}`).html(val.quantity);
            let element = ` <div class="col-12" id="row_product_${key}">
                                <div class="row" style="font-size: 18px; background: #${strStyle}; padding: 10px 0; border-bottom: 1px solid white;">
                                    <div class="col-4">
                                        ${val.info.name} ${val.info.category}
                                        <input type="hidden" name="product[]" value="${key}">
                                        <input type="hidden" name="category[]" value="${val.info.id_category}">
                                    </div>
                                    <div class="col-2" align="center">
                                        Q ${val.info.price}
                                        <input type="hidden" name="price[]" value="${val.info.price}">
                                    </div>
                                    <div class="col-2" align="center">
                                        ${val.quantity}
                                        <input type="hidden" name="quantity[]" value="${val.quantity}">
                                    </div>
                                    <div class="col-2" align="center">
                                        Q ${total}
                                    </div>
                                    <div class="col-2" align="center">
                                        <button class="btn btn-outline-danger" type="button" onclick="deleteCartProduct(${key})"><i class="fa fa-trash"></i></button>
                                    </div>
                                </div>
                            </div>`;
            $(`#cntAllProducts`).append(element);
        }

        drawTotalCart();
    }
    else if(Object.keys(objCart).length == 0) {
        $(".cnt-cart").removeClass("cnt-cart-show");
        drawCountCart();
    }
}

function drawTotalCart()
{
    let objCart = JSON.parse(localStorage.getItem('cart'));
    let total = 0;
    for(let key in objCart) {
        const val = objCart[key];
        if(typeof val.info.price !== 'undefined') {
            total += (val.quantity * 1) * (val.info.price);
        }
    }
    $(`#totalCart`).html(`Q ${total}`);

    drawCountCart();
}

function minCart(intProduct = 0)
{
    if(typeof intProduct !== "undefined") {
        let vExist = $(`#product_${intProduct}`).val();
        if(vExist * 1 > 0) {
            vExist -= 1;
            $(`#product_${intProduct}`).val(vExist);
            $(`#badge_${intProduct}`).html(vExist);
            setObjCart(intProduct);
        }
        else {
            deleteCartProduct(intProduct);
        }
    }
}

function sumCart(intProduct = 0, boolBadge = false)
{
    if(typeof intProduct !== "undefined") {
        let vExist = $(`#product_${intProduct}`).val();
        let newValue = (vExist * 1);
        newValue += 1;
        $(`#product_${intProduct}`).val(newValue);
        if(boolBadge) {
            $(`#badge_${intProduct}`).html(newValue);
        }
        addCart(intProduct);
    }
}

function addCart(intProduct = 0)
{
    if(typeof intProduct !== "undefined") {
        setObjCart(intProduct);
        //validar si existe solamente cambiar el valor
        //validar si no existe creo la fila dentro del div de carrito
    }
}

function deleteCartProduct(intProduct = 0)
{
    if(typeof intProduct !== "undefined") {
        let objCart = JSON.parse(localStorage.getItem('cart'));
        if(typeof objCart[intProduct] !== 'undefined'){
            delete objCart[intProduct];
            localStorage.setItem('cart', JSON.stringify(objCart));
            $(`#row_product_${intProduct}`).remove();
            $(`#product_${intProduct}`).val(0);
            $(`#badge_${intProduct}`).html(0);
            drawProductsInCart();
        }
    }
}

function drawCountCart()
{
    let element = document.getElementById("cntAllProducts");
    let intTotal = getCount(element, false);
    if(intTotal > 0) {
        let objCart = JSON.parse(localStorage.getItem('cart'));
        if(Object.keys(objCart).length > 0) {
            intTotal = 0;
            for(let key in objCart) {
                let val = objCart[key];
                intTotal += (val.quantity * 1);
            }
        }
    }
    let elementTotal = `<i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            Detalles de la compra
                        <span id="intCartDetail" class="badge badge-light">${intTotal}</span>`;
    $(`#btnDetailQuotation`).html(elementTotal);

    //esto hará que cuente las filas dentro de mi div final de carrito y muestre cuantos productos tengo en carrito
}

function showClientShop()
{
    $(`#cntClientShop`).css({
        'display': 'block',
        '-webkit-animation': 'slide-down .3s ease-out',
        '-mox-animation': 'slide-down .3s ease-out',
    });
}

function goToTab(intProduct = 0)
{
    if(typeof intProduct !== "undefined") {
        $(`#tab_category_${intProduct}`).click();
    }
}

function clearCart()
{
    localStorage.clear();
    location.reload();
}
