@include('tags')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('header')
        @yield('public_styles')
    </head>
    <body class="backgroundLogin">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-10 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
                    <form method="POST" action="{{ route('login') }}" class="formLogin">
                        @csrf
                        <div class="row justify-content-center">
                            <h2>INGRESA</h2>
                        </div>
                        <div class="row">
                            <div class="input-field col-sm-12">
                                <label for="email">
                                    Correo
                                </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">@</span>
                                    </div>
                                    <input id="email" type="email" class="form-control validate @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                </div>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <label for="email">
                                    Contraseña
                                </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">@</span>
                                    </div>
                                    <input id="password" type="password" class="form-control validate @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12 m4 offset-m4">
                                <label>
                                    <input type="checkbox" class="filled-in" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                                    <span>Recordarme</span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m8 offset-m2">
                                <center>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Ingresar') }}
                                    </button>
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m8 offset-m2">
                                @if (Route::has('password.request'))
                                    <center>
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('¿Perdió su contraseña?') }}
                                        </a>
                                    </center>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m8 offset-m2">
                                @if (Route::has('register'))
                                    <center>
                                        <a class="btn btn-link" href="{{ route('register') }}">
                                            {{ __('¿Eres Nuevo?') }}
                                        </a>
                                    </center>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
