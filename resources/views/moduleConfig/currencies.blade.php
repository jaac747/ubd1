@include('tags')
@yield('header')
@yield('menu')
<div class="col-12 col-md-10 offset-md-1">
    <div class="col-12" id="cntWindowConfig">
        @if(session()->has('flash'))
            <div class="col-12" style="margin-top: 25px">
                <div class="alert alert-success">
                    {{ session('flash') }}
                </div>
            </div>
        @endif
            <div class="col-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Ícono</th>
                        <th>Valor</th>
                        <th>Activo</th>
                        <th> Opciones <i class="fa fa-shapes"></i> </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($currencies))
                        @foreach($currencies AS $key => $value)
                            <tr>
                                <td>{{$value->name_currency}}</td>
                                <td>{{$value->icon}}</td>
                                <td>{{$value->fee}}</td>
                                <td>{{$value->active}}</td>
                                <td>
                                    <button id="btnEdit_{{$value->id}}" class="btn btn-success-outline" type="button" data-toggle="modal" data-target="#mdlEdit_{{$value->id}}" style="margin: 0 8px;">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <div class="modal fade" id="mdlEdit_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlEdit_{{$value->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">Editar Moneda "{{ $value->name_currency }}"</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                <form action="{{ route('configuration.editCurrencies') }}" method="POST">
                                                    <div class="modal-body">
                                                        @csrf
                                                        <input value="{{ $value->id }}" name="id" type="hidden" />
                                                        <div class="col-12">
                                                            <p>
                                                                Nombre
                                                            </p>
                                                            <input class="form-control" name="name" value="{{$value->name_currency}}" />
                                                        </div>
                                                        <div class="col-12">
                                                            <p>
                                                                Ícono
                                                            </p>
                                                            <input class="form-control" name="icon" value="{{$value->icon}}"/>
                                                        </div>
                                                        <div class="col-12">
                                                            <p>
                                                                Valor
                                                            </p>
                                                            <input class="form-control" name="fee" value="{{$value->fee}}" />
                                                        </div>
                                                        <div class="col-12">
                                                            <p>
                                                                Activo
                                                            </p>
                                                            <select class="form-control" name="active">
                                                                <option value="Y">
                                                                    Si
                                                                </option>
                                                                <option value="N">
                                                                    No
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                                                            Cancelar
                                                            <i class="fas fa-times"></i>
                                                        </button>
                                                        <button class="btn btn-success-outline">
                                                            Actualizar
                                                            <i class="fas fa-wrench"></i>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <button id="btnDelete_{{$value->id}}" class="btn btn-danger-outline" type="button" data-toggle="modal" data-target="#mdlDelete_{{$value->id}}" style="margin: 0 8px;">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <div class="modal fade" id="mdlDelete_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlDelete_{{$value->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header"><h5 class="modal-title">¿Desea borrar la moneda "{{ $value->name_currency }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                <form action="{{ route('configuration.deleteCurrencies') }}" method="POST">
                                                    <div class="modal-footer">
                                                        @csrf
                                                        <input value="{{ $value->id }}" name="id" type="hidden" />
                                                        <button class="btn btn-outline-danger">
                                                            Borrar
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-success-outline" data-dismiss="modal">
                                                            Cancelar
                                                            <i class="fas fa-times"></i>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" align="center">No hay datos a mostrar</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="col-12" id="addModule" style="margin: 20px 0;">
                <div class="container">
                    <div class="row justify-content-md-center">
                        <div class="col col-lg-2">
                            <button id="add" class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#mdlAdd">
                                Añadir
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>


<div class="modal fade" id="mdlAdd" tabindex="-1" role="dialog" aria-labelledby="aMdlAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Añadir Red Social</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('configuration.createCurrencies') }}" method="POST">
                <div class="modal-body">
                    @csrf
                    <div class="container">
                        <div class="col-12">
                            <p>
                                Nombre
                            </p>
                            <input class="form-control" name="name" />
                        </div>
                        <div class="col-12">
                            <p>
                                Ícono
                            </p>
                            <input class="form-control" name="icon" />
                        </div>
                        <div class="col-12">
                            <p>
                                Valor
                            </p>
                            <input class="form-control" name="fee" />
                        </div>
                        <div class="col-12">
                            <p>
                                Activo
                            </p>
                            <select class="form-control" name="active">
                                <option value="Y">
                                    Si
                                </option>
                                <option value="N">
                                    No
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                        Cancelar
                        <i class="fas fa-times"></i>
                    </button>
                    <button class="btn btn-success-outline">
                        Crear
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
