@include('tags')
@yield('header')
@yield('menu')
<style>
    #cntWindowConfig {
        overflow: auto;
    }
</style>
<div class="col-12 col-md-10 offset-md-1">
    <div class="col-12" id="cntWindowConfig">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-modules" role="tab" aria-controls="nav-home" aria-selected="true" style="padding: 7px 12px !important;">
                    Módulos
                </a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-window" role="tab" aria-controls="nav-profile" aria-selected="false" style="padding: 7px 12px !important;">
                    Ventanas por Módulo
                </a>
            </div>
        </nav>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-modules" role="tabpanel" aria-labelledby="nav-home-tab">
                @if(session()->has('flash'))
                    <div class="col-12" style="margin-top: 25px">
                        <div class="alert alert-success">
                            {{ session('flash') }}
                        </div>
                    </div>
                @endif
                <div class="col-12">
                    <table id="tblModules" class="table table-striped">
                        <thead>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Super Usuario
                            </th>
                            <th>
                                Opciones
                                <i class="fa fa-shapes"></i>
                            </th>
                        </thead>
                        <tbody id="tBodyModules">
                        @php
                            if(!empty($modules)){
                        @endphp
                                @foreach ($modules AS $key => $value)
                                    <tr>
                                        <td> {{$value->name_module}} </td>
                                        <td> {{$value->only_webmaster}} </td>
                                        <td>
                                            <button id="btnEdit_{{$value->id}}" class="btn btn-success-outline" type="button" data-toggle="modal" data-target="#mdlEditModule_{{$value->id}}" style="margin: 0 8px;">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <div class="modal fade" id="mdlEditModule_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlEditModule_{{$value->id}}" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">Editar Módulo {{ $value->name_module }}</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                        <form action="{{ route('configuration.editModule') }}" method="POST">
                                                            <div class="modal-body">
                                                                @csrf
                                                                <input value="{{ $value->id }}" name="module" type="hidden" />
                                                                <div class="container">
                                                                    <div class="col-12">
                                                                        <p>
                                                                            Nombre del módulo
                                                                        </p>
                                                                        <input id="name_module" class="form-control" name="name" value="{{ $value->name_module }}" />
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <p>
                                                                            ¿Webmaster?
                                                                        </p>
                                                                        <select class="form-control" name="wb">
                                                                            <option value="Y" @php ($value->only_webmaster == 'Y') ? print "selected": print ""; @endphp>
                                                                                Si
                                                                            </option>
                                                                            <option value="N" @php ($value->only_webmaster == 'N') ? print "selected": print ""; @endphp>
                                                                                No
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                                                                    Cancelar
                                                                    <i class="fas fa-times"></i>
                                                                </button>
                                                                <button class="btn btn-success-outline">
                                                                    Actualizar
                                                                    <i class="fas fa-wrench"></i>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <button id="btnDelete_{{$value->id}}" class="btn btn-danger-outline" type="button" data-toggle="modal" data-target="#mdlDeleteModule_{{$value->id}}" style="margin: 0 8px;">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <div class="modal fade" id="mdlDeleteModule_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlDeleteModule_{{$value->id}}" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">¿Desea borrar el módulo "{{ $value->name_module }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                        <form action="{{ route('configuration.deleteModule') }}" method="POST">
                                                            <div class="modal-footer">
                                                                @csrf
                                                                <input value="{{ $value->id }}" name="module" type="hidden" />
                                                                <button class="btn btn-outline-danger">
                                                                    Borrar
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-success-outline" data-dismiss="modal">
                                                                    Cancelar
                                                                    <i class="fas fa-times"></i>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                        @php
                            }
                            else {
                        @endphp
                                    <tr>
                                        <td colspan="3">
                                            No hay información a mostrar
                                        </td>
                                    </tr>
                        @php
                            }
                        @endphp
                        </tbody>
                    </table>
                </div>
                <div class="col-12" id="addModule" style="margin: 20px 0;">
                    <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col col-lg-2">
                                <button id="addModule" class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#mdlAddModule">
                                    Añadir
                                    <i class="fa fa-plus-circle"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="nav-window" role="tabpanel" aria-labelledby="nav-profile-tab">
                @if(session()->has('flash-detail'))
                    <div class="col-12" style="margin-top: 25px">
                        <div class="alert alert-success">
                            {{ session('flash-detail') }}
                        </div>
                    </div>
                @endif
                <div class="col-12">
                    <table id="tblModules" class="table table-striped">
                        <thead>
                            <th> Nómbre de la ventana </th>
                            <th> Módulo </th>
                            <th> Ruta </th>
                            <th> Descripcíon </th>
                            <th> Opciones <i class="fa fa-shapes"></i> </th>
                        </thead>
                        <tbody id="tBodyModules">
                        @php
                            if(!empty($windows)){
                                foreach ($windows AS $key => $value) {
                        @endphp
                                    <tr>
                                        <td> {{ $value->name_window }} </td>
                                        <td> {{ $value->name_module }} </td>
                                        <td> {{ $value->route }} </td>
                                        <td> {{ $value->description }} </td>
                                        <td>
                                            <button id="btnEdit_{{$value->id}}" class="btn btn-success-outline" type="button" data-toggle="modal" data-target="#mdlEditDetailModule_{{$value->id}}" style="margin: 0 8px;">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <div class="modal fade" id="mdlEditDetailModule_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlEditDetailModule_{{$value->id}}" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">Editar Ventana {{ $value->name_window }}</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                        <form action="{{ route('configuration.editDetailModule') }}" method="POST">
                                                            <div class="modal-body">
                                                                @csrf
                                                                <input value="{{ $value->id }}" name="window" type="hidden" />
                                                                <div class="col-12">
                                                                    <p> Nombre de la ventana </p>
                                                                    <input id="name_window" class="form-control" name="name_window" value="{{ $value->name_window }}" />
                                                                </div>
                                                                <div class="col-12">
                                                                    <p> Asignar a Módulo </p>
                                                                    <select id="sltModuleAdd" name="int_module" class="form-control">
                                                                        <option value="0">Selecione uno</option>
                                                                        @foreach($modules AS $key => $valueModule)
                                                                            <option value="{{ $valueModule->id }}" @php ($valueModule->id == $value->id_module) ? print "selected": print ""; @endphp>
                                                                                {{ $valueModule->name_module }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-12">
                                                                    <p>Ruta </p>
                                                                    <input id="route" class="form-control" name="route" value="{{ $value->route }}" />
                                                                </div>
                                                                <div class="col-12">
                                                                    <p> Descripción de la ventana </p>
                                                                    <textarea class="form-control" name="description">{{ $value->description }}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                                                                    Cancelar
                                                                    <i class="fas fa-times"></i>
                                                                </button>
                                                                <button class="btn btn-success-outline">
                                                                    Actualizar
                                                                    <i class="fas fa-wrench"></i>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <button id="btnDelete_{{$value->id}}" class="btn btn-danger-outline" type="button" data-toggle="modal" data-target="#mdlDeleteDetailModule_{{$value->id}}" style="margin: 0 8px;">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <div class="modal fade" id="mdlDeleteDetailModule_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlDeleteDetailModule_{{$value->id}}" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header"><h5 class="modal-title">¿Desea borrar la ventana "{{ $value->name_window }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                        <form action="{{ route('configuration.deleteDetailModule') }}" method="POST">
                                                            <div class="modal-footer">
                                                                @csrf
                                                                <input value="{{ $value->id }}" name="window" type="hidden" />
                                                                <button class="btn btn-outline-danger">
                                                                    Borrar
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-success-outline" data-dismiss="modal">
                                                                    Cancelar
                                                                    <i class="fas fa-times"></i>
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                        @php
                                }
                            }
                            else {
                        @endphp
                        <tr>
                            <td colspan="3">
                                No hay información a mostrar
                            </td>
                        </tr>
                        @php
                            }
                        @endphp
                        </tbody>
                    </table>
                </div>
                <div class="col-12" id="addModule" style="margin: 20px 0;">
                    <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col col-lg-2">
                                <button id="addDetailModule" class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#mdlAddDetailModule">
                                    Añadir
                                    <i class="fa fa-plus-circle"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlAddModule" tabindex="-1" role="dialog" aria-labelledby="aMdlAddModule" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Añadir Módulo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('configuration.createModule') }}" method="POST">
                <div class="modal-body">
                    @csrf
                    <div class="container">
                        <div class="col-12">
                            <p>
                                Nombre del módulo
                            </p>
                            <input id="name_module" class="form-control" name="name" />
                        </div>
                        <div class="col-12">
                            <p>
                                ¿Webmaster?
                            </p>
                            <select class="form-control" name="wb">
                                <option value="Y">
                                    Si
                                </option>
                                <option value="N">
                                    No
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                        Cancelar
                        <i class="fas fa-times"></i>
                    </button>
                    <button class="btn btn-success-outline">
                        Crear
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlAddDetailModule" tabindex="-1" role="dialog" aria-labelledby="aMdlAddDetailModule" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Añadir Ventana</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('configuration.createDetailModule') }}" method="POST">
                <div class="modal-body">
                    @csrf
                    <div class="container">
                        <div class="col-12">
                            <p> Nombre de la ventana </p>
                            <input id="name_window" class="form-control" name="name_window" />
                        </div>
                        <div class="col-12">
                            <p> Asignar a Módulo </p>
                            <select id="sltModuleAdd" name="int_module" class="form-control">
                                <option value="0">Selecione uno</option>
                                @foreach($modules AS $key => $value)
                                    <option value="{{ $value->id }}">
                                        {{ $value->name_module }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <p>Ruta</p>
                            <input id="route" class="form-control" name="route" />
                        </div>
                        <div class="col-12">
                            <p> Descripción de la ventana </p>
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                        Cancelar
                        <i class="fas fa-times"></i>
                    </button>
                    <button class="btn btn-success-outline">
                        Crear
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/configJS.js') }}" defer></script>
