<!DOCTYPE html>
<html>


<body>
@extends ('master')
@section('content')
    <div class="login-area">
        <div class="bg-image">
            <div class="login-signup">
                <div class="container">
                    <div class="login-header">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="login-logo">
                                   
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="login-details">
                                    <ul class="nav nav-tabs navbar-right">
                                        
                                        <li ><a data-toggle="tab" href="#login">Login</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="tab-content">
 
                        <div id="login" class="tab-pane active">
                            <div class="login-inner">
                                <div class="title">
                                    <h1>Iniciar Sesion</h1>
                                </div>
                                <div class="login-form">
                                    <form method="post" action="{{url('/verificar')}}">
                                        {{csrf_field()}}
                                        <div class="form-details">
                                            <label class="user">
                                                <input type="email" name="email" placeholder="Ejemplo@web.com" id="email">
                                            </label>
                                            <label class="pass">
                                                <input type="password" name="password" placeholder="Contraseña" id="password">
                                            </label>
                                        </div>
                                        <button type="submit" class="form-btn">Aceptar</button>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>

