{{--@include('tags')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @yield('header')
        @yield('public_styles')
    </head>
    <body>
    <section id="portraitInitial" class="portrait">
        <header class="header">
            <figure class="logotype">
                <img src="images/logotype.png" class="imgLogotype" alt="Logotype Software Ride"/>
            </figure>
            <nav class="menu">
                <ul>
                    <li>
                        <a href="/">Inicio</a>
                    </li>
                    <li>
                        <a href="/public/services">Servicios</a>
                    </li>
                    <li>
                        <a href="/public/about-us">Sobre Nosotros</a>
                    </li>
                    <li>
                        <a href="/public/contact-us">Contáctanos</a>
                    </li>
                </ul>
            </nav>
        </header>
    </section>
    </body>
</html>--}}
@include('tags')
@yield('header')
@yield('public_styles')
<style type="text/css">
    @font-face {
        font-family: Quicksand;
        src: url("{{asset('css/fonts/Quicksand/Quicksand-Regular.ttf')}}");
    }
    * {
        font-family: Quicksand, sans-serif;
    }
    h2 {
        color: #000;
        font-size: 26px;
        font-weight: 300;
        text-align: center;
        text-transform: uppercase;
        position: relative;
        margin: 30px 0 80px;
    }
    h2 b {
        color: #ffc000;
    }
    h2::after {
        content: "";
        width: 100px;
        position: absolute;
        margin: 0 auto;
        height: 4px;
        background: rgba(0, 0, 0, 0.2);
        left: 0;
        right: 0;
        bottom: -20px;
    }
    .carousel {
        margin: 50px auto;
        padding: 0 70px;
    }
    .carousel .item {
        min-height: 330px;
        text-align: center;
        overflow: hidden;
    }
    .carousel .item .img-box {
        height: 160px;
        width: 100%;
        position: relative;
    }
    .carousel .item img {
        max-width: 100%;
        max-height: 100%;
        display: inline-block;
        position: absolute;
        bottom: 0;
        margin: 0 auto;
        left: 0;
        right: 0;
    }
    .carousel .item h4 {
        font-size: 18px;
        margin: 10px 0;
    }
    .carousel .item .btn {
        color: #333;
        border-radius: 0;
        font-size: 11px;
        text-transform: uppercase;
        font-weight: bold;
        background: none;
        border: 1px solid #ccc;
        padding: 5px 10px;
        margin-top: 5px;
        line-height: 16px;
    }
    .carousel .item .btn:hover, .carousel .item .btn:focus {
        color: #fff;
        background: #000;
        border-color: #000;
        box-shadow: none;
    }
    .carousel .item .btn i {
        font-size: 14px;
        font-weight: bold;
        margin-left: 5px;
    }
    .carousel .thumb-wrapper {
        text-align: center;
    }
    .carousel .thumb-content {
        padding: 15px;
    }
    .carousel .carousel-control {
        height: 100px;
        width: 40px;
        background: none;
        margin: auto 0;
        background: rgba(0, 0, 0, 0.2);
    }
    .carousel .carousel-control i {
        font-size: 30px;
        position: absolute;
        top: 50%;
        display: inline-block;
        margin: -16px 0 0 0;
        z-index: 5;
        left: 0;
        right: 0;
        color: rgba(0, 0, 0, 0.8);
        text-shadow: none;
        font-weight: bold;
    }
    .carousel .item-price {
        font-size: 13px;
        padding: 2px 0;
    }
    .carousel .item-price strike {
        color: #999;
        margin-right: 5px;
    }
    .carousel .item-price span {
        color: #86bd57;
        font-size: 110%;
    }
    .carousel .carousel-control.left i {
        margin-left: -3px;
    }
    .carousel .carousel-control.left i {
        margin-right: -3px;
    }
    .carousel .carousel-indicators {
        bottom: -50px;
    }
    .carousel-indicators li, .carousel-indicators li.active {
        width: 10px;
        height: 10px;
        margin: 4px;
        border-radius: 50%;
        border-color: transparent;
    }
    .carousel-indicators li {
        background: rgba(0, 0, 0, 0.2);
    }
    .carousel-indicators li.active {
        background: rgba(0, 0, 0, 0.6);
    }
    .star-rating li {
        padding: 0;
    }
    .star-rating i {
        font-size: 14px;
        color: #ffc000;
    }

    /********************* Shopping Demo-3 **********************/
    .product-grid3{font-family:Quicksand;text-align:center;position:relative;z-index:1}
    .product-grid3:before{content:"";height:81%;width:100%;background:#fff;border:1px solid rgba(0,0,0,.1);opacity:0;position:absolute;top:0;left:0;z-index:-1;transition:all .5s ease 0s}
    .product-grid3:hover:before{opacity:1;height:100%}
    .product-grid3 .product-image3{position:relative}
    .product-grid3 .product-image3 a{display:block}
    .product-grid3 .product-image3 img{width:100%;height:200px}
    .product-grid3 .pic-1{opacity:1;transition:all .5s ease-out 0s}
    .product-grid3:hover .pic-1{opacity:0}
    .product-grid3 .pic-2{position:absolute;top:0;left:0;opacity:0;transition:all .5s ease-out 0s}
    .product-grid3:hover .pic-2{opacity:1}
    .product-grid3:hover .btn-outline-info{opacity:1}
    .product-grid3 .social{width:150px;padding:0;margin:0 auto;list-style:none;opacity:0;position:absolute;right:0;left:0;bottom:-23px;transform:scale(0);transition:all .3s ease 0s}
    .product-grid3:hover .social{opacity:1;transform:scale(1)}
    .product-grid3:hover .product-discount-label,.product-grid3:hover .product-new-label,.product-grid3:hover .title{opacity:0}
    .product-grid3 .social li{display:inline-block}
    .product-grid3 .social li a{color:#e67e22;background:#fff;font-size:18px;line-height:50px;width:50px;height:50px;border:1px solid rgba(0,0,0,.1);border-radius:50%;margin:0 2px;display:block;transition:all .3s ease 0s}
    .product-grid3 .social li a:hover{background:#e67e22;color:#fff}
    .product-grid3 .product-discount-label,.product-grid3 .product-new-label{background-color:#e67e22;color:#fff;font-size:17px;padding:2px 10px;position:absolute;right:10px;top:10px;transition:all .3s}
    .product-grid3 .product-content{z-index:-1;padding:15px;text-align:left}
    .product-grid3 .title{font-size:14px;text-transform:capitalize;margin:0 0 7px;transition:all .3s ease 0s}
    .product-grid3 .title a{color:#414141}
    .product-grid3 .price{color:#000;font-size:16px;letter-spacing:1px;font-weight:600;margin-right:2px;display:inline-block}
    .product-grid3 .price span{color:#909090;font-size:14px;font-weight:500;letter-spacing:0;text-decoration:line-through;text-align:left;display:inline-block;margin-top:-2px}
    .product-grid3 .rating{padding:0;margin:-22px 0 0;list-style:none;text-align:right;display:block}
    .product-grid3 .rating li{color:#ffd200;font-size:13px;display:inline-block}
    .product-grid3 .rating li.disable{color:#dcdcdc}
    @media only screen and (max-width:1200px){.product-grid3 .rating{margin:0}
    }
    @media only screen and (max-width:990px){.product-grid3{margin-bottom:30px}
        .product-grid3 .rating{margin:-22px 0 0}
    }
    @media only screen and (max-width:359px){.product-grid3 .rating{margin:0}
    }





    .floating-menu{border-radius:100px;z-index:999;padding-top:10px;padding-bottom:10px;left:0;position:fixed;display:inline-block;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);transform:translateY(-50%)}.main-menu{margin:0;padding-left:0;list-style:none}.main-menu li a{display:block;padding:20px;color:#fff;border-radius:50px;position:relative;-webkit-transition:none;-o-transition:none;transition:none}.main-menu li a:hover{background:rgba(244,244,244,.3)}.menu-bg{background-image:-webkit-linear-gradient(top,#1C5E91 0,#167699 100%);background-image:-o-linear-gradient(top,#1C5E91 0,#167699 100%);background-image:-webkit-gradient(linear,left top,left bottom,from(#1C5E91),to(#167699));background-image:linear-gradient(to bottom,#1C5E91 0,#167699 100%);background-repeat:repeat-x;position:absolute;width:100%;height:100%;border-radius:50px;z-index:-1;top:0;left:0;-webkit-transition:.1s;-o-transition:.1s;transition:.1s}.ripple{position:relative;overflow:hidden;transform:translate3d(0,0,0)}.ripple:after{content:"";display:block;position:absolute;width:100%;height:100%;top:0;left:0;pointer-events:none;background-image:radial-gradient(circle,#000 10%,transparent 10.01%);background-repeat:no-repeat;background-position:50%;transform:scale(10,10);opacity:0;transition:transform .5s,opacity 1s}.ripple:active:after{transform:scale(0,0);opacity:.2;transition:0s}



    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }


    body {
        background-position: center;
        background-repeat: repeat;
        background-size: 7%;
        background-color: #fff;
        overflow-x: hidden;
        transition: all 200ms linear;
    }

    .btnCart {
        position:absolute;
        top:5%;
        right:2%
    }

    .cnt-cart {
        width: 85%;
        height: 100%;
        border: 1px solid black;
        background-color: white;
        position: fixed;
        top: 0;
        right: -91%;
        overflow-y: auto;
        /* margin-right: 210px; */
        /* margin-right: -157px; */
        -webkit-transition: right 0.5s; /* Safari */
        transition: right 0.5s;
        -webkit-transition-timing-function: linear;
        transition-timing-function: linear;

        padding: 15px;
        z-index: 1050;
    }
    .cnt-cart div .row-plan{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .cnt-cart div .cnt-table{
        background-color: #f9f9f9;
        padding-top: 10px;
    }
    .cnt-cart div .cnt-table table tr td.gray{
        background-color: #eaeaea;
        color: #7c7c7c;
        text-align: center;
    }
    .cnt-cart div .cnt-table table tr td.other-gray{
        background-color: #9b9b9b;
        color: white;
        text-align: center;
        border: 1px solid #cccccc;
    }
    .cnt-cart div .cnt-table table tr td.device{
        background-color: #d9e2f3;
        color: black;
        font-size: 10px;
        text-align: center;
        border: 1px solid #cccccc;
    }
    .cnt-cart div .cnt-table table tr td{
        background-color: white;
        color: #7c7c7c;
        border: 1px solid #cccccc;
        padding: 0px 10px 0px 10px;
        /*max-width: 50px !important;*/
    }
    .cnt-cart-show{
        right: 2px;
    }

    .rowCart {
        margin-top: 20px;
    }

    @-webkit-keyframes slide-down {
        0% { opacity: 0; -webkit-transform: translateY(-100%); }
        100% { opacity: 1; -webkit-transform: translateY(0); }
    }
    @-moz-keyframes slide-down {
        0% { opacity: 0; -moz-transform: translateY(-100%); }
        100% { opacity: 1; -moz-transform: translateY(0); }
    }

    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
        background: #6c757d !important;
    }
</style>
{{--<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Productos <b>más Vendidos</b></h2>
            <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">

                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="item carousel-item active">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="thumb-wrapper">
                                    <div class="img-box">
                                        <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                    </div>
                                    <div class="thumb-content">
                                        <h4>Apple iPad</h4>
                                        <p class="item-price"><strike>$400.00</strike> <span>$369.00</span></p>
                                        <a href="#" class="btn btn-primary">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="thumb-wrapper">
                                    <div class="img-box">
                                        <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                    </div>
                                    <div class="thumb-content">
                                        <h4>Sony Headphone</h4>
                                        <p class="item-price"><strike>$25.00</strike> <span>$23.99</span></p>
                                        <a href="#" class="btn btn-primary">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="thumb-wrapper">
                                    <div class="img-box">
                                        <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                    </div>
                                    <div class="thumb-content">
                                        <h4>Macbook Air</h4>
                                        <p class="item-price"><strike>$899.00</strike> <span>$649.00</span></p>
                                        <a href="#" class="btn btn-primary">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="thumb-wrapper">
                                    <div class="img-box">
                                        <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                    </div>
                                    <div class="thumb-content">
                                        <h4>Nikon DSLR</h4>
                                        <p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
                                        <a href="#" class="btn btn-primary">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item carousel-item">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="thumb-wrapper">
                                    <div class="img-box">
                                        <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                    </div>
                                    <div class="thumb-content">
                                        <h4>Bose Speaker</h4>
                                        <p class="item-price"><strike>$109.00</strike> <span>$99.00</span></p>
                                        <a href="#" class="btn btn-primary">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="thumb-wrapper">
                                    <div class="img-box">
                                        <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                    </div>
                                    <div class="thumb-content">
                                        <h4>Samsung Galaxy S8</h4>
                                        <p class="item-price"><strike>$599.00</strike> <span>$569.00</span></p>
                                        <a href="#" class="btn btn-primary">Add to Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>--}}

@if(!empty($products))
    <div class="row">
        @if(session()->has('flash'))
            <div class="col-12" style="margin-top: 25px">
                <div class="alert alert-success">
                    {{ session('flash') }}
                    <button class="btn btn-outline-success" type="button" onclick="clearCart()" style="margin: 10px 20px">
                        Aceptar
                        <i class="far fa-thumbs-up"></i>
                    </button>
                </div>
            </div>
        @endif
    </div>
    <div>
        <button id="btnDetailQuotation" type="button" class="btn btn-outline-info btnCart" onclick="showCart()">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            Detalles de la compra
            <span id="intCartDetail" class="badge badge-light">0</span>
        </button>
    </div>
    <div class="cnt-cart">
        <h3>
            DETALLE DE COMPRA
            <i class="fa fa-times cnt-cart-close" aria-hidden="true" onclick="closeCart()" style="float: right; cursor: pointer;"></i>
        </h3>
        <form method="post" action="{{ route('shop.cart') }}">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div style="margin-top: 25px">
                        <div class="col-12">
                            <div class="row" style="background: #9d9d9d; color: white">
                                <div class="col-4" style="border: 1px solid white; font-size: 25px" align="center">Producto</div>
                                <div class="col-2" style="border: 1px solid white; font-size: 25px" align="center">Precio</div>
                                <div class="col-2" style="border: 1px solid white; font-size: 25px" align="center">Cantidad</div>
                                <div class="col-2" style="border: 1px solid white; font-size: 25px" align="center">Total</div>
                                <div class="col-2" style="border: 1px solid white; font-size: 25px" align="center">Borrar</div>
                            </div>
                        </div>
                    </div>

                    <div id="cntAllProducts"></div>

                    <div style="margin-top: 25px; border-bottom: 1px solid black; margin-bottom: 20px;">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-8"><h3>Total Compra</h3></div>
                                <div class="col-4" align="center"><h4 id="totalCart">Q 0</h4></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <div id="cntInfoButtonsTest" align="center">
                        <button type="button" class="btn btn-outline-info" onclick="test();">
                            Iniciar Sesion
                            <i class="fas fa-check" aria-hidden="true"></i>
                        </button>   
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div id="cntInfoButtonsTest" align="center">
                        <button type="button" class="btn btn-outline-info" onclick="test2();">
                            Registrar
                            <i class="fas fa-check" aria-hidden="true"></i>
                        </button>   
                    </div>
                </div>
            </div>

            
   
            <script>

            function test(){
                window.location.href = "http://localhost:8000/reg";
            }

            </script>


            <script>

            function test2(){
                 window.location.href = "http://localhost:8000/register";
}

            </script>


            

            <div class="row">
                <div class="col-12">
                    <div id="cntInfoButtonsAddToCart" align="center">
                        <button type="button" class="btn btn-outline-info" onclick="showClientShop();">
                            Compra Como Invitado
                            <i class="fas fa-check" aria-hidden="true"></i>
                        </button>   
                    </div>
                </div>
            </div>
            

            <div class="row" style="margin-top: 25px;">
                <div class="col-12" id="cntClientShop" style="display: none;">

                    <h4 align="center">Datos del cliente</h4>
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <p>Nombre y Apellido</p>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                        <div class="col-12 col-md-5">
                            <p>Correo</p>
                            <input type="text" name="mail" class="form-control" required>
                        </div>
                        <div class="col-12 col-md-2">
                            <p>Teléfono</p>
                            <input type="text" name="phone" class="form-control" required>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-12">
                            <p>Dirección para entregar <br>(especifique detalles al repartidor)</p>
                            <textarea class="form-control" name="address"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 offset-md-8 col-md-4 offset-lg-10 col-lg-2">
                            <p>Código de cliente (si tiene uno)</p>
                            <input type="text" name="code" class="form-control">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-12" style="margin-top: 20px;">
                            <div align="center">
                                <button class="btn btn-outline-success">
                                    Comprar
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>              
            </div>
        </form>
    </div>

    <div class="container" style="margin-top: 50px;">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            @php $count = 0; @endphp
            @foreach($products AS $key => $v)
                <li class="nav-item">
                    <a class="nav-link @if($count <= 0) active @else @endif" id="tab_category_{{$key}}"
                       data-toggle="pill" href="#category_detail_{{$key}}"
                       role="tab" aria-controls="category_detail_{{$key}}" aria-selected="@if($count <= 0) true @else false @endif">
                        {{$v["category"]}}
                    </a>
                </li>
                @php $count += $count +1; @endphp
            @endforeach
        </ul>
        <div class="tab-content" id="pills-tabContent">
            @php $count = 0; @endphp
            @foreach($products AS $key => $v)
                <div class="tab-pane fade @if($count <= 0) show active @else @endif " id="category_detail_{{$key}}" role="tabpanel" aria-labelledby="tab_category_{{$key}}">
                    <h1 class="h1" id="" align="center">
                        <strong>
                            {{$v["category"]}}
                        </strong>
                    </h1>
                    @foreach($v["detail"] AS $ke => $va)
                        <h3 class="h3">
                            <strong>
                                {{$va["product"]}}
                            </strong>
                        </h3>
                        <div class="row">
                            @foreach($va["detail"] AS $key => $val)
                                <div class="col-md-3 col-sm-12">
                                    <div class="col-12" style="display: table; z-index: 9;">
                                        <button class="btn btn-info" type="button" style="float: right; border-radius: 40px;" onclick="sumCart({{$val->id}}, true)">
                                            <i class="fa fa-shopping-cart"></i>
                                            Añadir
                                            <span id="badge_{{ $val->id }}" class="badge badge-light">0</span>
                                        </button>
                                    </div>
                                    <div class="col-12">
                                        <div class="product-grid3" style="margin-top: -12px;">
                                            <div class="product-image3">
                                                <a href="#">
                                                    <img class="pic-1" src="{{url("var/images/{$val->image}")}}">
                                                    <img class="pic-2" src="{{url("var/images/{$val->image}")}}">
                                                    {{--<img class="pic-2" src="images/no_image.png">--}}
                                                </a>
                                                <ul class="social">
                                                    <span style="display: block;">
                                                            <button type="button" class="btn btn-outline-danger" style="display: inline-block; margin: 0; padding: 5px;" onclick="minCart({{$val->id}})">
                                                                <i class="fa fa-minus-circle"></i>
                                                            </button>
                                                            <input type="number" class="form-control" id="product_{{$val->id}}" style="display: inline-block; width: 50px;" onchange="addCart({{$val->id}})">
                                                            <button type="button" class="btn btn-success-outline" style="display: inline-block; margin: 0; padding: 5px;" onclick="sumCart({{$val->id}})">
                                                                <i class="fa fa-plus-circle"></i>
                                                            </button>
                                                    </span>
                                                </ul>
                                            </div>

                                            <div class="product-content">
                                                <h3 class="title"><a href="#">{{$val->name_product_detail}}</a></h3>
                                                <div class="price">
                                                    Q {{$val->price_unity}} @if($val->type_product_detail == 'unity') Unidad @else Libra  @endif
                                                </div>
                                                <input type="hidden" value="{{$val->price_unity}}" id="price_{{$val->id}}">
                                                <input type="hidden" value="{{$val->name_product_detail}}" id="name_{{$val->id}}">
                                                <input type="hidden" value="{{ $v["category"] }}" id="strCategory_{{$val->id}}">
                                                <input type="hidden" value="{{ $v["id_category"] }}" id="id_category_{{$val->id}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
                @php $count += $count +1; @endphp
            @endforeach
        </div>
    </div>

    <nav class="floating-menu">
        <ul class="main-menu">
            @foreach($products AS $key => $v)
                <li>
                    <a href="#" onclick="goToTab({{$key}})" class="ripple" data-toggle="tooltip" data-placement="top" title="{{$v["category"]}}">
                        <i class="{{ $v["icon"] }}"></i>
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="menu-bg"></div>
    </nav>


@else

@endif
