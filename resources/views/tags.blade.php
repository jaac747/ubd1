
@section('header')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="Software Ride">
    <meta property="og:description" content="Empresa dedicada al desarrollo de nuevas tecnologías para facilitar tu negocio y tener la información detallada de estos al alcanze de tu mano.">
    <meta property="og:image" content="images/logotype.png">
    <meta property="og:site_name" content="MarketGT"/>
    <meta property="og:url" content="https://www.softwareride.com">

    <title>Software Ride</title>
    <link rel="icon" href="images/icon_browser.png">


    <script src="{{ asset('js/jquery.min.js') }}" defer></script>
    <script src="{{ asset('js/core_nn.js') }}" defer></script>
    <script src="{{ asset('js/fontawesome/js/all.js') }}" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('js/public.js') }}" defer></script>
    <script src="{{ asset('plugins/datatables/jquery.datatables.min.js') }}" defer></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}" defer></script>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables/jquery.datatables.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/system.css') }}" rel="stylesheet">
@stop

@section('public_styles')
    <link href="{{ asset('css/styles_public.css') }}" rel="stylesheet">
@stop


@section('menu')
    <script src="{{ asset('js/menu.js') }}" defer></script>
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">

    <div class="hero-anime">

        <div class="navigation-wrap bg-light start-header start-style">
            <div>
                <div class="row">
                    <div class="col-md-11 offset-md-1">
                        <nav class="navbar navbar-expand-md navbar-light">

                            <a class="navbar-brand" href="{{ route('home') }}">
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1462889/logo.svg" alt="">
                            </a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto py-4 py-md-0">
                                    @php
                                    if(!empty(session('menu'))){
                                        foreach (session('menu')[0] AS $key => $value) {
                                            @endphp
                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4 active">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                            {{ $value["name"] }}
                                            @php
                                            if(!empty($value['detail'])){
                                            @endphp
                                            <i class="fa fa-sort-down"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            @php
                                                foreach ($value['detail'] AS $keyDetail => $valueDetail) {
                                            @endphp
                                            <a class="dropdown-item" href="{{ $valueDetail->route }}">
                                                {{ $valueDetail->name_window }}
                                            </a>
                                            @php
                                                }
                                            @endphp
                                        </div>
                                            @php
                                            }
                                            else {
                                            @endphp
                                        </a>
                                            @php
                                            }
                                            @endphp

                                    </li>
                                            @php
                                        }
                                    }
                                    @endphp
                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                        <a class="nav-link dropdown-toggle" href="/dashboard" role="button">
                                            Dashboard
                                            <i class="fas fa-chart-pie"></i>
                                        </a>
                                    </li>
                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                            Mi cuenta
                                            <i class="fa fa-sort-down"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{route('myAccount')}}">
                                                Mi perfil
                                            </a>
                                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                Cerrar sesión
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <a style="margin: auto 25px;" class="navbar-brand" href="{{ route('myAccount') }}" data-toggle="tooltip" data-placement="bottom" title="Mi perfil">
                                <img src="{{ url('images/profile.png') }}" alt="">
                            </a>

                        </nav>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="section full-height">
            <div class="absolute-center">
                <div class="section">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <h1><span>B</span><span>o</span><span>o</span><span>t</span><span>s</span><span>t</span><span>r</span><span>a</span><span>p</span> <span>4</span><br>
                                    <span>m</span><span>e</span><span>n</span><span>u</span></h1>
                                <p>scroll for nav animation</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section mt-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div id="switch">
                                    <div id="circle"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="https://themeforest.net/user/ig_design/portfolio"      class="link-to-portfolio" target=”_blank”></a>--}}
        <div id="cntAll" class="cntAll">
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
@stop
