@include('tags')
@yield('header')
@yield('menu')
<div style="margin-top: 100px">
    <h2 class="titleRoute">Actualizar datos de mi cuenta</h2>
    <div class="container">
        @if(session()->has('flash-detail'))
            <div class="col-12" style="margin-top: 25px">
                <div class="alert alert-success">
                    {{ session('flash-detail') }}
                </div>
            </div>
        @endif
        <form method="POST" action="{{route('user.updateUser')}}">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row" style="margin-top: 15px">
                    <div class="col-12 col-md-4">
                        <p>
                            Nombre de Usuario
                        </p>
                    </div>
                    <div class="col-12 col-md-8">
                        <input type="hidden" name="user" value="{{$arrDataUser->id}}" />
                        <input type="text" class="form-control" name="name" value="{{$arrDataUser->name}}" />
                    </div>
                </div>
                <div class="row" style="margin-top: 15px">
                    <div class="col-12 col-md-4">
                        <p>
                            Correo electrónico
                        </p>
                    </div>
                    <div class="col-12 col-md-8">
                        <input type="email" class="form-control" name="email" value="{{$arrDataUser->email}}" />
                    </div>
                </div>
                <div class="row" style="margin-top: 15px">
                    <div class="col-12 col-md-4">
                        <p>
                            Contraseña <br>(si la deja vacía mantendrá su contraseña actual)
                        </p>
                    </div>
                    <div class="col-12 col-md-8">
                        <input type="text" class="form-control" name="password" value="" />
                    </div>
                </div>
                <div class="row" style="margin-top: 15px">
                    <div class="col-12">
                        <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="col col-lg-2">
                                    <button class="btn btn-success-outline">
                                        Actualizar
                                        <i class="fas fa-wrench"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
