@include('tags')
@yield('header')
@yield('menu')

<div style="margin-top: 100px">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <table class="table table-striped table-bordered" id="tblResume">
                <thead>
                    <tr>
                        <th>No. de cotización</th>
                        <th>Nombre del cliente</th>
                        <th>Teléfono del cliente</th>
                        <th>Correo del cliente</th>
                        <th>Valor cotizado</th>
                        <th>Fecha de cotización</th>
                        <th>Estado de la cotización</th>
                        <th> Opciones <i class="fa fa-shapes"></i> </th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($arrEstimates))
                        @foreach($arrEstimates AS $key => $value)
                            <tr>
                                <td>{{$value->correlative}}</td>
                                <td>{{$value->client_name}}</td>
                                <td>{{$value->client_phone}}</td>
                                <td>{{$value->client_email}}</td>
                                <td>{{$value->total_estimate}}</td>
                                <td>{{$value->created_at}}</td>
                                <td>
                                    {{$value->status}}
                                    <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#mdlEstimateStatus_{{$value->id}}" style="float: right">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <div class="modal fade" id="mdlEstimateStatus_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlEstimateStatus_{{$value->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">Datos de la cotización "{{ $value->correlative }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                <form method="post" action="{{ route('cart.editStatus') }}">
                                                    <div class="modal-body">
                                                        <div class="col-12">
                                                            @csrf
                                                            <p>Actualizar estado</p>
                                                            <input type="hidden" name="estimate" value="{{$value->id}}">
                                                            <select class="form-control" name="status">
                                                                <option value="Enviado">Enviado</option>
                                                                <option value="Entregado">Entregado</option>
                                                                <option value="Pagado">Pagado</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger-outline" data-dismiss="modal">
                                                            Cancelar
                                                            <i class="fas fa-times"></i>
                                                        </button>
                                                        <button class="btn btn-success-outline">
                                                            Actualizar
                                                            <i class="fas fa-wrench"></i>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mdlEstimate_{{$value->id}}">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                    <div class="modal fade" id="mdlEstimate_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlEstimate_{{$value->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">Datos de la cotización "{{ $value->correlative }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                <div class="modal-body">
                                                    <div class="col-12">

                                                        <p><strong>Detalles de la compra: </strong></p>
                                                        <p>
                                                            @foreach($arrDetail AS $k => $v)
                                                                @if($v["id_estimate"] == $value->id)
                                                                    {{ $v["detail"] }}
                                                                @endif
                                                            @endforeach
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success-outline" data-dismiss="modal">
                                                        Aceptar
                                                        <i class="fa fa-check"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" align="center">
                                No hay información de cotizaciones a mostrar
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="{{ asset('js/home_blade.js') }}" defer></script>
