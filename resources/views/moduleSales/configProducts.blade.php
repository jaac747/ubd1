@include('tags')
@yield('header')
@yield('menu')
<div class="col-12 col-md-10 offset-md-1">
    <div class="col-12">
        <h2 class="titleRoute">Administración detallada de los productos</h2>
        <table id="tblProductsDetail" class="table table-striped">
            <thead>
                <tr>
                    <td>Nombre</td>
                    <td>Tipo de producto</td>
                    <td>Imagen</td>
                    
                    <td>Precio</td>
                    <td>Cantidad existente</td>
                    

                    <th>Opciones <i class="fa fa-shapes"></i></th>
                    
                </tr>
            </thead>
            <tbody>
                @if(!empty($products))
                    @foreach($products AS $key => $value)
                        <tr>
                            <td>{{$value->name}}</td>
                            <td>@if($value->type_product_detail == "unity") Unidad @else Peso @endif</td>
                            <td>
                                <img src="{{url("var/images/{$value->image}")}}" alt="Imagen de producto" style="height: 35px; max-height: 35px; border-radius: 10%;">
                            </td>
                            <td>{{$value->price}}</td>
                            
                            <td @if($value->boolRed) style="color: red" @else style="" @endif>{{$value->quantity}}</td>
                            <td>
                                <button id="btnEdit_{{$value->id}}" class="btn btn-success-outline" type="button" data-toggle="modal" data-target="#mdlEdit_{{$value->id}}" style="margin: 0 8px;">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <div class="modal fade" id="mdlEdit_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="aMdlEdit_{{$value->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Añadir Detalle</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                            </div>
                                            <form action="{{ route('product.editProduct') }}" method="POST" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    @csrf
                                                    <div class="container">
                                                        <input type="hidden" value="{{$value->id}}" name="id">
                                                        <div class="col-12">
                                                            <p> Nombre del producto </p>
                                                            <input class="form-control" name="name" value="{{$value->name}}" />
                                                        </div>
                                                        <div class="col-12">
                                                            <p> Tipo de producto </p>
                                                            <select class="form-control" name="type_product">
                                                                <option value="unity" @if($value->type_product_detail == "unity") selected @endif>
                                                                    Unidad
                                                                </option>
                                                                <option value="weight"@if($value->type_product_detail == "weight") selected @endif>
                                                                    Peso
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-12">
                                                            <p> Seleccione producto </p>
                                                            <select class="form-control" name="product">
                                                                @foreach($prd AS $kp => $vp)
                                                                    <option value="{{$vp->id}}" @if($vp->id == $value->id_product) selected @endif>{{$vp->name_product}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-12">
                                                            <p> Precio </p>
                                                            <input type="text" class="form-control" name="fee" value="{{$value->price}}" />
                                                        </div>
                                                        <div class="col-12">
                                                            <p> Cantidad </p>
                                                            <input type="text" class="form-control" name="quantity" value="{{$value->quantity}}" />
                                                        </div>
                                                        <div class="col-12">
                                                            <p> Oferta </p>
                                                            <input type="text" class="form-control" name="offer" value="{{$value->offer}}" />
                                                        </div>
                                                        <div class="col-12">
                                                            <p> Temporada </p>
                                                            <select class="form-control" name="provider">
                                                                <option value="0">Seleccione uno</option>
                                                                @if(!empty($providers))
                                                                    @foreach($providers AS $kp => $vp)
                                                                        <option value="{{$vp->id}}" @if($vp->id == $value->id_provider) selected @endif>{{$vp->name_provider}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-12">
                                                            <p> Activo </p>
                                                            <select class="form-control" name="active">
                                                                <option value="Y" @if($value->active == "Y") selected @endif>
                                                                    Si
                                                                </option>
                                                                <option value="N" @if($value->active == "N") selected @endif>
                                                                    No
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-12">
                                                            <p>Imagen</p>
                                                            <input type="file" class="form-control" name="image">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                                                        Cancelar
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                    <button class="btn btn-success-outline">
                                                        Actualizar
                                                        <i class="fas fa-wrench"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                                <button id="btnDelete_{{$value->id}}" class="btn btn-danger-outline" type="button" data-toggle="modal" data-target="#mdlDeleteCategory_{{$value->id}}" style="margin: 0 8px;">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <div class="modal fade" id="mdlDeleteCategory_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlDeleteCategory_{{$value->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">¿Desea eliminar el producto "{{ $value->name }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                            <form action="{{ route('product.deleteProduct') }}" method="POST">
                                                <div class="modal-footer">
                                                    @csrf
                                                    <input value="{{ $value->id }}" name="id" type="hidden" />
                                                    <button class="btn btn-outline-danger">
                                                        Borrar
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-success-outline" data-dismiss="modal">
                                                        Cancelar
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                @if(!empty($social))
                                    @foreach($social AS $keyS => $valueS)
                                        <a href="{{$valueS->url_api}}{{$phone}}{{$value->phone}}" target="_blank"><i class="{{$valueS->icon}}"></i></a>
                                    @endforeach
                                @else
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" align="center">No hay información a mostrar</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <div class="col-12">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col col-lg-2">
                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mdlAdd"> Crear Nuevo Detalle <i class="fa fa-plus"></i> </button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="mdlAdd" tabindex="-1" role="dialog" aria-labelledby="aMdlAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Añadir Detalle</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <form action="{{ route('product.createProduct') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="container">
                        <div class="col-12">
                            <p> Nombre del producto </p>
                            <input class="form-control" name="name" />
                        </div>
                        <div class="col-12">
                            <p> Tipo de producto </p>
                            <select class="form-control" name="type_product">
                                <option value="unity">
                                    Unidad
                                </option>
                                <option value="weight">
                                    Peso
                                </option>
                            </select>
                        </div>
                        <div class="col-12">
                            <p> Seleccione producto </p>
                            <select class="form-control" name="product">
                                @foreach($prd AS $kp => $vp)
                                    <option value="{{$vp->id}}">{{$vp->name_product}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <p> Precio </p>
                            <input type="text" class="form-control" name="fee" />
                        </div>
                        <div class="col-12">
                            <p> Cantidad </p>
                            <input type="text" class="form-control" name="quantity" />
                        </div>
                        <div class="col-12">
                            <p> Oferta </p>
                            <input type="text" class="form-control" name="offer" />
                        </div>
                        <div class="col-12">
                            <p> Temporada </p>
                            <select class="form-control" name="provider">
                                <option value="0">Seleccione uno</option>
                                @if(!empty($providers))
                                    @foreach($providers AS $kp => $vp)
                                        <option value="{{$vp->id}}">{{$vp->name_provider}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-12">
                            <p> Activo </p>
                            <select class="form-control" name="active">
                                <option value="Y">
                                    Si
                                </option>
                                <option value="N">
                                    No
                                </option>
                            </select>
                        </div>
                        <div class="col-12">
                            <p>Imagen</p>
                            <input type="file" class="form-control" name="image">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                        Cancelar
                        <i class="fas fa-times"></i>
                    </button>
                    <button class="btn btn-success-outline">
                        Crear
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/home_blade.js') }}" defer></script>
