@include('tags')
@yield('header')
@yield('menu')
<div class="col-12 col-md-10 offset-md-1">

<div class="col-12 col-md-10 offset-md-1">
    <div class="col-12">
        <h2 class="titleRoute">Configuración de categorías y productos</h2>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-modules" role="tab" aria-controls="nav-home" aria-selected="true" style="padding: 7px 12px !important;">
                    Categorías
                </a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-window" role="tab" aria-controls="nav-profile" aria-selected="false" style="padding: 7px 12px !important;">
                    Productos
                </a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-modules" role="tabpanel" aria-labelledby="nav-home-tab">
                @if(session()->has('flash'))
                    <div class="col-12" style="margin-top: 25px">
                        <div class="alert alert-success">
                            {{ session('flash') }}
                        </div>
                    </div>
                @endif
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nombre de la categoría</th>
                        <th>Imágen</th>
                        <th>Ícono</th>
                        <th>Opciones <i class="fa fa-shapes"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        if(!empty($categories)){
                    @endphp
                    @foreach($categories AS $key => $value)
                        <tr>
                            <td> {{ $value->name_category }}</td>
                            <td style="padding: 5px 0 !important;">
                                <img src="{{url("var/images/{$value->image_category}")}}" alt="Imágen de categoría" style="height: 35px; max-height: 35px; border-radius: 10%;" />
                            </td>
                            <td>
                                <i class="{{$value->icon}}"></i>
                            </td>
                            <td>
                                <button id="btnEdit_{{$value->id}}" class="btn btn-success-outline" type="button" data-toggle="modal" data-target="#mdlEditCategory_{{$value->id}}" style="margin: 0 8px;">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <div class="modal fade" id="mdlEditCategory_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlEditModule_{{$value->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">Editar Categoría {{ $value->name_category }}</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                            <form action="{{ route('category.editCategory') }}" method="POST" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    @csrf
                                                    <input value="{{ $value->id }}" name="category" type="hidden" />
                                                    <div class="container">
                                                        <div class="col-12">
                                                            <p>
                                                                Nombre de la categoría
                                                            </p>
                                                            <input id="name_category" class="form-control" name="name" value="{{ $value->name_category }}" />
                                                        </div>
                                                        <div class="col-12">
                                                            <p>
                                                                Ícono
                                                            </p>
                                                            <input class="form-control" name="icon" value="{{ $value->icon }}" />
                                                        </div>
                                                        <div class="col-12">
                                                            <p>
                                                                Activo
                                                            </p>
                                                            <select class="form-control" name="active">
                                                                <option value="Y" @php ($value->active == 'Y') ? print "selected": print ""; @endphp>
                                                                Si
                                                                </option>
                                                                <option value="N" @php ($value->active == 'N') ? print "selected": print ""; @endphp>
                                                                No
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div class="col-12">
                                                            <p> Imagen </p>
                                                            <input type="file" class="form-control" name="image" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                                                        Cancelar
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                    <button class="btn btn-success-outline">
                                                        Actualizar
                                                        <i class="fas fa-wrench"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <button id="btnDelete_{{$value->id}}" class="btn btn-danger-outline" type="button" data-toggle="modal" data-target="#mdlDeleteCategory_{{$value->id}}" style="margin: 0 8px;">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <div class="modal fade" id="mdlDeleteCategory_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlDeleteCategory_{{$value->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">¿Desea borrar la categoría "{{ $value->name_category }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                            <form action="{{ route('category.deleteCategory') }}" method="POST">
                                                <div class="modal-footer">
                                                    @csrf
                                                    <input value="{{ $value->id }}" name="category" type="hidden" />
                                                    <button class="btn btn-outline-danger">
                                                        Borrar
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-success-outline" data-dismiss="modal">
                                                        Cancelar
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @php
                        }
                        else {
                    @endphp
                    <tr>
                        <td colspan="4" align="center">No hay información a mostrar</td>
                    </tr>
                    @php
                        }
                    @endphp
                    </tbody>
                </table>
                <div class="col-12">
                    <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col col-lg-2">
                                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mdlAddCategory"> Crear Nueva Categoría <i class="fa fa-plus"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-window" role="tabpanel" aria-labelledby="nav-profile-tab">
                @if(session()->has('flash'))
                    <div class="col-12" style="margin-top: 25px">
                        <div class="alert alert-success">
                            {{ session('flash') }}
                        </div>
                    </div>
                @endif
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nombre del producto</th>
                        <th>Imágen</th>
                        <th>Tipo de producto</th>
                        <th>Categoría</th>
                        <th>Moneda</th>
                        <th>Opciones <i class="fa fa-shapes"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                        if(!empty($products)){
                    @endphp
                    @foreach($products AS $key => $value)
                        <tr>
                            <td> {{ $value->name_product }} </td>
                            <td>
                                <img src="{{url("var/images/{$value->image_product}")}}" alt="Imágen de producto" style="height: 35px; max-height: 35px; border-radius: 10%;" />
                            </td>
                            <td> {{ $value->type_product }} </td>
                            <td> {{ $value->name_category }} </td>
                            <td> {{ $value->icon }} </td>
                            <td>
                                <button id="btnEdit_{{$value->id}}" class="btn btn-success-outline" type="button" data-toggle="modal" data-target="#mdlEditProduct_{{$value->id}}" style="margin: 0 8px;">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <div class="modal fade" id="mdlEditProduct_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlEditProduct_{{$value->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Editar Producto "{{ $value->name_product }}"</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                                            </div>
                                            <form action="{{ route('category.editProduct') }}" method="POST" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    @csrf
                                                    <div class="container">
                                                        <input type="hidden" name="product" value="{{ $value->id }}" />
                                                        <div class="row" style="padding: 15px 0 !important;">
                                                            <div class="col-12 col-md-6">
                                                                <p> Nombre del producto </p>
                                                                <input class="form-control" name="name" value="{{ $value->name_product }}" />
                                                            </div>
                                                            <div class="col-12 col-md-6">
                                                                <p> Activo </p>
                                                                <select class="form-control" name="active">
                                                                    <option value="Y" @php ($value->active == 'Y') ? print "selected": print ""; @endphp>
                                                                        Si
                                                                    </option>
                                                                    <option value="N" @php ($value->active == 'N') ? print "selected": print ""; @endphp>
                                                                        No
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="padding: 15px 0 !important;">
                                                            <div class="col-12">
                                                                <p> Descripción del producto </p>
                                                                <textarea class="form-control" name="description">{{ $value->description_product }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="padding: 15px 0 !important;">
                                                            <div class="col-12">
                                                                <p> Imagen </p>
                                                                <input type="file" class="form-control" name="image"/>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="padding: 15px 0 !important;">
                                                            <div class="col-12 col-md-6">
                                                                <p> Categoría </p>
                                                                <select class="form-control" name="category">
                                                                    <option value="0">
                                                                        Seleccione categoría
                                                                    </option>
                                                                    @php if(!empty($categories)) { @endphp
                                                                    @foreach($categories AS $keyCategory => $valueCategory)
                                                                        <option value="{{ $valueCategory->id }}" @php ($value->id_category == $valueCategory->id) ? print "selected": print ""; @endphp>{{ $valueCategory->name_category }}</option>
                                                                    @endforeach
                                                                    @php } @endphp
                                                                </select>
                                                            </div>
                                                            <div class="col-12 col-md-6">
                                                                <p> Moneda </p>
                                                                <select class="form-control" name="currency">
                                                                    <option value="0">
                                                                        Selecciona moneda
                                                                    </option>
                                                                    @php if(!empty($currencies)) { @endphp
                                                                    @foreach($currencies AS $keyCurrency => $valCurrency)
                                                                        <option value="{{ $valCurrency->id }}" @php ($value->currency == $valCurrency->id) ? print "selected": print ""; @endphp>{{ $valCurrency->icon }}</option>
                                                                    @endforeach
                                                                    @php } @endphp
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="padding: 15px 0 !important;">
                                                            <div class="col-12 col-md-6">
                                                                <p> Tipo de producto </p>
                                                                <select class="form-control" name="type_product">
                                                                    <option value="Simple" @php ($value->type_product == 'simple') ? print "selected": print ""; @endphp>
                                                                        Simple
                                                                    </option>
                                                                    <option value="Union" @php ($value->type_product == 'union') ? print "selected": print ""; @endphp>
                                                                        Completo (incluye cuotas)
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="col-12 col-md-6">
                                                                <p> Mínimo existencias </p>
                                                                <input name="min_exist" class="form-control" value="{{$value->min_exist}}" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                                                        Cancelar
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                    <button class="btn btn-success-outline">
                                                        Actualizar
                                                        <i class="fas fa-wrench"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <button id="btnDelete_{{$value->id}}" class="btn btn-danger-outline" type="button" data-toggle="modal" data-target="#mdlDeleteProduct_{{$value->id}}" style="margin: 0 8px;">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <div class="modal fade" id="mdlDeleteProduct_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlDeleteProduct_{{$value->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">¿Desea borrar el producto "{{ $value->name_product }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                            <form action="{{ route('category.deleteProduct') }}" method="POST">
                                                <div class="modal-footer">
                                                    @csrf
                                                    <input value="{{ $value->id }}" name="product" type="hidden" />
                                                    <button class="btn btn-outline-danger">
                                                        Borrar
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-success-outline" data-dismiss="modal">
                                                        Cancelar
                                                        <i class="fas fa-times"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @php
                        }
                        else {
                    @endphp
                    <tr>
                        <td colspan="6" align="center">No hay información a mostrar</td>
                    </tr>
                    @php
                        }
                    @endphp
                    </tbody>
                </table>
                <div class="col-12">
                    <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col col-lg-2">
                                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mdlAddProduct"> Crear Nuevo Producto <i class="fa fa-plus"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlAddCategory" tabindex="-1" role="dialog" aria-labelledby="aMdlAddCategory" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Añadir Categoría</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <form action="{{ route('category.createCategory') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="container">
                        <div class="col-12">
                            <p> Nombre de la categoría </p>
                            <input id="name_category" class="form-control" name="name" />
                        </div>
                        <div class="col-12">
                            <p> Ícono </p>
                            <input class="form-control" name="icon" />
                        </div>
                        <div class="col-12">
                            <p> Activo </p>
                            <select class="form-control" name="active">
                                <option value="Y">
                                    Si
                                </option>
                                <option value="N">
                                    No
                                </option>
                            </select>
                        </div>
                        <div class="col-12">
                            <p> Imagen </p>
                            <input type="file" class="form-control" name="image" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                        Cancelar
                        <i class="fas fa-times"></i>
                    </button>
                    <button class="btn btn-success-outline">
                        Crear
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="mdlAddProduct" tabindex="-1" role="dialog" aria-labelledby="aMdlAddProduct" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Añadir Producto</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <form action="{{ route('category.createProduct') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="container">
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12 col-md-6">
                                <p> Nombre del producto </p>
                                <input class="form-control" name="name" />
                            </div>
                            <div class="col-12 col-md-6">
                                <p> Activo </p>
                                <select class="form-control" name="active">
                                    <option value="Y">
                                        Si
                                    </option>
                                    <option value="N">
                                        No
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12">
                                <p> Descripción del producto </p>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12">
                                <p> Imagen </p>
                                <input type="file" class="form-control" name="image" />
                            </div>
                        </div>
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12 col-md-6">
                                <p> Categoría </p>
                                <select class="form-control" name="category">
                                    <option value="0">
                                        Seleccione categoría
                                    </option>
                                    @php if(!empty($categories)) { @endphp
                                    @foreach($categories AS $keyCategory => $valueCategory)
                                        <option value="{{ $valueCategory->id }}">{{ $valueCategory->name_category }}</option>
                                    @endforeach
                                    @php } @endphp
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <p> Moneda </p>
                                <select class="form-control" name="currency">
                                    <option value="0" >
                                        Selecciona moneda
                                    </option>
                                    @php if(!empty($currencies)) { @endphp
                                    @foreach($currencies AS $keyCurrency => $valCurrency)
                                        <option value="{{ $valCurrency->id }}">{{ $valCurrency->icon }}</option>
                                    @endforeach
                                    @php } @endphp
                                </select>
                            </div>
                        </div>
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12 col-md-6">
                                <p> Tipo de producto </p>
                                <select class="form-control" name="type_product">
                                    <option value="Simple">
                                        Simple
                                    </option>
                                    <option value="Union">
                                        Completo (incluye cuotas)
                                    </option>
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <p> Mínimo existencias </p>
                                <input name="min_exist" class="form-control" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                        Cancelar
                        <i class="fas fa-times"></i>
                    </button>
                    <button class="btn btn-success-outline">
                        Crear
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
