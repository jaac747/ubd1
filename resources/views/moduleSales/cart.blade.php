@include('tags')
@yield('header')
@yield('menu')
<div class="col-12 col-md-10 offset-md-1">

    <div class="tabs">
        <div >
            <div class="row">
                <div class="col-xl-2">
                    <ul class="nav nav-pills nav-stacked flex-column">
                        <li class="active"><a href="#tab_a" data-toggle="pill">TAB 1</a></li>
                        <li><a href="#tab_b" data-toggle="pill">TAB 2</a></li>
                        <li><a href="#tab_c" data-toggle="pill">TAB 3</a></li>
                    </ul>
                </div>
                <div class="col-xl-6">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_a">
                            <h3>First tab with soft transitioning effect.</h3>
                            <p>American Builders Inc. is your full service general contractor. We have been helping
                                clients throughout Eastern North Carolina with their construction needs since 1996.
                                We take pride in understanding our clients' needs, making their construction experience
                                as worry free as possible and only delivering a finished product that will withstand the
                                test of time. </p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                                bibendum laoreet.</p>
                        </div>
                        <div class="tab-pane" id="tab_b">
                            <h3>Second tab with soft transitioning effect.</h3>
                            <p>We maintain a reputation for effective communication and collaboration between our
                                team and clients to minimize surprises and ensure precise project delivery. Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Aenean euismod
                                bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra
                                justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque
                                penatibus et magnis dis parturient montes.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                                bibendum laoreet.</p>
                        </div>
                        <div class="tab-pane" id="tab_c">
                            <h3>Third tab with soft transitioning effect.</h3>
                            <p>Thank you for taking the time to consider American Builders Inc. as your general
                                contractor for any and all of your commercial, residential, insurance restoration and metal
                                building needs. Please feel free to explore our website, and be sure to click on the
                                Facebook link at the bottom of this page and like us on Facebook.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                                bibendum laoreet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
