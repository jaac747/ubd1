@include('tags')
@yield('header')
@yield('menu')
<div class="col-12 col-md-10 offset-md-1">

    <div class="col-12 col-md-10 offset-md-1">
        <div class="col-12">
            <h2 class="titleRoute">Configuración de temporadas</h2>
        </div>

        <div class="col-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Activo</th>
                        

                        <th>Opciones <i class="fa fa-shapes"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($providers))
                        @foreach($providers AS $key => $value)
                            <tr>
                                <td>{{$value->name_provider}}</td>
                                <td>{{$value->address}}</td>
                                <td>{{$value->link_share}}</td>
                                <td>
                                    <button id="btnEdit_{{$value->id}}" class="btn btn-success-outline" type="button" data-toggle="modal" data-target="#mdlEdit{{$value->id}}" style="margin: 0 -8px;">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    <div class="modal fade" id="mdlEdit{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlEdit{{$value->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">Editar Temporada {{ $value->name_provider }}</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                <form action="{{ route('providers.edit') }}" method="POST">
                                                    <div class="modal-body">
                                                        @csrf
                                                        <input value="{{ $value->id }}" name="id" type="hidden" />
                                                        <div class="container">
                                                            <div class="row" style="padding: 15px 0 !important;">
                                                                <div class="col-12">
                                                                    <p> Nombre Temporada </p>
                                                                    <input class="form-control" name="name" value="{{$value->name_provider}}" />
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding: 15px 0 !important;">
                                                                <div class="col-12">
                                                                    <p> Descripción </p>
                                                                    <input class="form-control" name="address" {{$value->address}}>
                                                                </div>

                                                            </div>
                                                            <div class="row" style="padding: 15px 0 !important;">
                                                                <div class="col-12">
                                                                    <p> Activo </p>
                                                                    <select class="form-control" name="social">
                                                                        <option value="Y" @if($value->link_share == "Y") selected @else @endif>
                                                                            Si
                                                                        </option>
                                                                        <option value="N" @if($value->link_share == "N") selected @else @endif>
                                                                            No
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                                <div class="row" style="padding: 15px 0 !important;">
+                                                                <div class="col-12 col-md-6">
+                                                                    <p> Fecha de Inicio </p>
+                                                                    <input class="form-control" type='date' name="datei" value='{{$value->datei}}'>
+                                                                </div>
+                                                                <div class="col-12 col-md-6">
+                                                                    <p> Fecha Fin </p>
+                                                                    <input class="form-control" type='date' name="datef" value='{{$value->datef}}'>
+                                                               </div>
+                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                                                            Cancelar
                                                            <i class="fas fa-times"></i>
                                                        </button>
                                                        <button class="btn btn-success-outline">
                                                            Actualizar
                                                            <i class="fas fa-wrench"></i>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <button id="btnDelete_{{$value->id}}" class="btn btn-danger-outline" type="button" data-toggle="modal" data-target="#mdlDelete_{{$value->id}}" style="margin: 0 8px;">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <div class="modal fade" id="mdlDelete_{{$value->id}}" tabindex="-1" role="dialog" aria-labelledby="mdlDelete_{{$value->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">¿Desea borrar al proveedor "{{ $value->name_provider }}"?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
                                                <form action="{{ route('providers.delete') }}" method="POST">
                                                    <div class="modal-footer">
                                                        @csrf
                                                        <input value="{{ $value->id }}" name="id" type="hidden" />
                                                        <button class="btn btn-outline-danger">
                                                            Borrar
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-success-outline" data-dismiss="modal">
                                                            Cancelar
                                                            <i class="fas fa-times"></i>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4" align="center">No hay información para mostrar</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

        <div class="col-12">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col col-lg-2">
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#mdlAdd"> Crear Temporada <i class="fa fa-plus"></i> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="mdlAdd" tabindex="-1" role="dialog" aria-labelledby="amdlAdd" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Crear Temporada</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <form action="{{ route('providers.create') }}" method="POST">
                <div class="modal-body">
                    @csrf
                    <div class="container">
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12">
                                <p> Nombre de la temporada </p>
                                <input class="form-control" name="name" />
                            </div>
                        </div>
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12">
                                <p> Descripción </p>
                                <input class="form-control" name="address">
                            </div>
                        </div>
                  <!--   <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12">
                                <p> Correo </p>
                                <input class="form-control" name="mail">
                            </div>
                        </div>
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12 col-md-6">
                                <p> Teléfono 1 </p>
                                <input class="form-control" name="phone">
                            </div>
                            <div class="col-12 col-md-6">
                                <p> Teléfono 2 </p>
                                <input class="form-control" name="phone_two" >
                            </div>
                        </div>-->
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12">
                                <p> Activo </p>
                                <select class="form-control" name="social">
                                    <option value="Y">
                                        Si
                                    </option>
                                    <option value="N">
                                        No
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="padding: 15px 0 !important;">
                            <div class="col-12 col-md-6">
                                <p> Fecha de Inicio </p>
                                <input class="form-control" type='date' name="datei">
                            </div>
                            <div class="col-12 col-md-6">
                                <p> Fecha Fin </p>
                                <input class="form-control" type='date' name="datef">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">
                        Cancelar
                        <i class="fas fa-times"></i>
                    </button>
                    <button class="btn btn-success-outline">
                        Crear
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
