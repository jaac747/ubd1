@include('tags')
@yield('header')
@yield('menu')
<div class="col-12">
    <div class="col-12">
        <form type="GET" action="{{ route('dashboard') }}" enctype="multipart/form-data">
            <div class="row">
                <div class="col-12 col-md-3">
                    <p>Desde</p>
                    <input class="form-control" name="from" type="date">
                </div>
                <div class="col-12 col-md-3">
                    <p>Hasta</p>
                    <input class="form-control" name="to" type="date">
                </div>
                <div class="col-12 col-md-3">
                    <button class="btn btn-outline-info" style="margin-top: 35px;">
                        Filtar
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
    {{--@php dd($dashboard); @endphp--}}
    @if(!empty($dashboard))

    @endif
</div>

<div id="piechart"></div>

<script type="application/javascript" defer>
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Work', 8],
            ['Friends', 2],
            ['Eat', 2],
            ['TV', 2],
            ['Gym', 2],
            ['Sleep', 8]
        ]);

        // Optional; add a title and set the width and height of the chart
        var options = {'title':'My Average Day', 'width':550, 'height':400};

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
</script>
