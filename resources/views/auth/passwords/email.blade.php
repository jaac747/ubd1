@include('tags')
@yield('header')
@yield('public_styles')
<div class="backgroundLogin">
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-10 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                    <form method="POST" action="{{ route('password.email') }}" class="formLogin">
                        @csrf
                        <h2>Recuperar contraseña</h2>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">
                                Correo electrónico
                            </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar contraseña
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

