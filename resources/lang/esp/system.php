<?php

return [
    'nameSystem' => 'MarketGT',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
];
