<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
/*class Contact extends Model
{
    public $table = 'providers';
    protected $primaryKey = 'id';

    public $fillable = [
        'name_provider', 'address', 'phone', 'phone_two', 'link_share', 'link_share_two', 'email', 'created_at','updated_at'
    ];
}*/

class Contact extends Model
{
    public $sequence  = 'SEQUENCE_PRODUCT_DETAIL';
    public $table = 'sales_products_detail';
    protected $primaryKey = 'id';

    public $fillable = [
        'id_product', 'name_product_detail', 'type_product_detail', 'price_unity', 'price_weight', 'price_before_update', 'active',
         'description','offer','time_limit','date_of_expiry','quantity_exist_unity','quantity_reservation_unity','quantity_exist_weight',
         'quantity_reservation_weight','code_inventory','created_at','updated_at','icon','image','id_provider'
    ];
}