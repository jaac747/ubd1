<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public $sequence  = 'SEQUENCE_PRODUCTS';
    protected $table = 'sales_products';
    protected $guarded = ['id'];
}
