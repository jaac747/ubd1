<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    public $sequence  = 'SEQUENCE_CLIENTS';
    protected $table = 'clients';
    protected $guarded = ['id'];
}
