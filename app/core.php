<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class core extends Model
{
    //
    public function getOptionsMenu($intIDUser = '', $boolAdmin = false)
    {
        if(!empty($intIDUser)){
            if($boolAdmin) {
                $arrInfoUser = DB::table('core_modules')
                    ->join('core_modules_detail', 'core_modules.id', '=', 'core_modules_detail.id_module')
                    ->get([
                        'core_modules.id',
                        'core_modules.name_module',
                        'core_modules_detail.id',
                        'core_modules_detail.id_module',
                        'core_modules_detail.name_window',
                        'core_modules_detail.route',
                    ]);
                if( !empty($arrInfoUser) ) {
                    return $arrInfoUser;
                }

                return [];
            }
            else {
                $arrInfoUser = DB::select("
                                SELECT
                                    MD.id,
                                    M.name_module,
                                    MD.id_module,
                                    MD.name_window,
                                    MD.route
                                    FROM users AS U
                                        INNER JOIN users_profiles AS P
                                            ON U.id_user_profile = P.id
                                        LEFT JOIN users_profiles_detail AS PD
                                            ON P.id = PD.id_user_profile
                                        INNER JOIN core_modules_detail AS MD
                                            ON PD.id_modules_detail = MD.id
                                        LEFT JOIN core_modules M
                                            ON MD.id_module = M.id
                                WHERE U.id = '{$intIDUser}'
                ");

                if(!empty($arrInfoUser)){
                    return $arrInfoUser;
                }

                return [];
            }
        }
        else {
            return [];
        }
    }
}
