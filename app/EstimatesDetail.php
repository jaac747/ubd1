<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstimatesDetail extends Model
{
    public $sequence  = 'SEQUENCE_ESTIMATES_DETAIL';
    protected $table = 'ESTIMATES_PRODUCTS_DETAIL';
    protected $guarded = ['id'];

    public $sortable = [
        'id',
        'id_estimate',
        'id_product',
        'total_individual_product_estimate',
        'quantity',
        'category',
        'price',
    ];
}
