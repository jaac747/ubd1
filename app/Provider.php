<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    public $sequence  = 'SEQUENCE_PROVIDERS';
    protected $table = 'providers';
    protected $guarded = ['id'];
}
