<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class tbl_usuario extends Model implements AuthenticatableContract
{
    public $table = "users";
    use Authenticatable;
    //public $table ="users";
}