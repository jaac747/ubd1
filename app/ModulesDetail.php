<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModulesDetail extends Model
{
    public $sequence  = 'SEQUENCE_MODULES_DETAIL';
    protected $table = 'core_modules_detail';
    protected $guarded = ['id'];
}
