<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estimates extends Model
{
    public $sequence  = 'SEQUENCE_ESTIMATES';
    protected $table = 'ESTIMATES';
    protected $guarded = ['id'];
}
