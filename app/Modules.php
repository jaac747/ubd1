<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    public $sequence  = 'SEQUENCE_MODULES';
    protected $table = 'core_modules';
    protected $guarded = ['id'];
}
