<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_usuario;
use \Illuminate\Support\Facades\Validator ;
use Auth;

class LoginUsuarioController extends Controller
{



    public  function  LoginUsuario()
    {
        $credenciales = $this->Validate(request(),
        [
            'email' => 'email',
            'password' => 'min:6'
        ]);

        if (Auth::attempt($credenciales))
        {
            
            return redirect()->to('tarjeta');
        }
        else
        {
            return back()
                ->withErrors(['email'=>trans('auth.failed')])
                ->withInput(request(['email']));
        }
    }
}
