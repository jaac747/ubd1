<?php

namespace App\Http\Controllers;

use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Providers extends Controller
{
    public function init()
    {
        $arrProviders = DB::select('SELECT * FROM providers');
        return view('moduleSales.providers', ['providers' => $arrProviders]);
    }

    public function create(Request $request)
    {
        if(!empty($request->name)){
            Provider::create([
                'name_provider' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone,
                'phone_two' => $request->phone_two,
                'link_share' => $request->social,
                'link_share_two' => $request->social,
                'email' => $request->email,
                'datei' => $request->datei,
                'datef' => $request->datef,
            ]);
            return back()->with('flash', 'Producto creado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo crear el producto, contacte a soporte');
        }
    }

    public function delete(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('providers')->where('id', $request->id)->delete();
            return back()->with('flash', 'Proveedor borrado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo eliminar al proveedor');
        }
    }

    public function edit(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('providers')->where('id', $request->id)->update([
                'name_provider' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone,
                'phone_two' => $request->phone_two,
                'link_share' => $request->social,
                'link_share_two' => $request->social,
                'email' => $request->email,
                'datei' => $request->datei,
                'datef' => $request->datef,
            ]);
            return back()->with('flash', 'Proveedor editado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar al proveedor');
        }
    }
}
