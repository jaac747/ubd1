<?php

namespace App\Http\Controllers;

use App\Currencies;
use App\Modules;
use App\ModulesDetail;
use App\Phones;
use App\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class ConfigWebMasterController extends Controller
{
    public function init()
    {
        $arrModules = DB::select('SELECT id, name_module, only_webmaster FROM core_modules');
        $arrWindows = DB::select('SELECT MD.id, M.id AS id_module, M.name_module, MD.name_window, MD.route, MD.description_name_window AS "description"
                                            FROM core_modules_detail MD
                                            INNER JOIN core_modules M
                                                ON MD.id_module = M.id');
        return view('moduleConfig.config', ['modules' => $arrModules, 'windows' => $arrWindows]);
    }

    public function createModule(Request $request)
    {
        if(!empty($request->name)){
            $strName = strtolower($request->name);
            $strQuickName = str_replace(' ', '_', $strName);
            Modules::create([
                'name_module' => utf8_encode($request->name),
                'quick_name_module' => utf8_encode($strQuickName),
                'only_webmaster' => $request->wb,
            ]);
            return back()->with('flash', 'Módulo creado correctamente');
        }
        else {
            /*$response = array(
                  'status' => 'success',
                  'msg' => 'Setting created successfully',
              );
              return Response::json($response);*/
            return back()->with('flash', 'No se pudo crear el módulo, contacte a soporte');
        }
    }

    public function editModule(Request $request)
    {
        if(!empty($request->module)) {
            $strName = strtolower($request->name);
            $strQuickName = str_replace(' ', '_', $strName);
            DB::table('core_modules')
                ->where('id', $request->module)
                ->update([
                    'name_module' => utf8_encode($request->name),
                    'quick_name_module' => utf8_encode($strQuickName),
                    'only_webmaster' => $request->wb,
                ]);
            return back()->with('flash', 'Información actualizada correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar el módulo');
        }
    }

    public function deleteModule(Request $request)
    {
        if(!empty($request->module)) {
            DB::table('core_modules')->where('id', $request->module)->delete();
            return back()->with('flash', 'Módulo borrado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar el módulo');
        }
    }

    public function createDetailModule(Request $request)
    {
        if(!empty($request->name_window)){
            $name_window = strtolower($request->name_window);
            $strQuickName = str_replace(' ', '_', $name_window);
            $strRoute = str_replace(' ', '-', $name_window);
            ModulesDetail::create([
                'name_window' => utf8_encode($request->name_window),
                'quick_name_window' => utf8_encode($strQuickName),
                'id_module' => $request->int_module,
                'route' => utf8_encode($request->route),
                'description_name_window' => $request->description,
            ]);
            return back()->with('flash-detail', 'Ventana creada correctamente');
        }
        else {
            return back()->with('flash-detail', 'No se pudo crear la ventana, contacte a soporte');
        }
    }

    public function editDetailModule(Request $request)
    {
        if(!empty($request->window)) {
            $name_window = strtolower($request->name_window);
            $strQuickName = str_replace(' ', '_', $name_window);
            $strRoute = str_replace(' ', '-', $name_window);
            DB::table('core_modules_detail')
                ->where('id', $request->window)
                ->update([
                    'name_window' => utf8_encode($request->name_window),
                    'quick_name_window' => utf8_encode($strQuickName),
                    'id_module' => utf8_encode($request->int_module),
                    'route' => utf8_encode($request->route),
                    'description_name_window' => $request->description,
                ]);
            return back()->with('flash-detail', 'Información actualizada correctamente');
        }
        else {
            return back()->with('flash-detail', 'No se pudo editar el módulo');
        }
    }

    public function deleteDetailModule(Request $request)
    {
        if(!empty($request->window)) {
            DB::table('core_modules_detail')->where('id', $request->window)->delete();
            return back()->with('flash-detail', 'Módulo borrado correctamente');
        }
        else {
            return back()->with('flash-detail', 'No se pudo editar el módulo');
        }
    }

    public function socialMedia()
    {
        $arrSocial = DB::select('SELECT id, name_social, url_api, active, icon FROM social_media');
        return view('moduleConfig.social', ['social' => $arrSocial]);
    }

    public function createSocial(Request $request)
    {
        if(!empty($request->name)){
            Social::create([
                'name_social' => $request->name,
                'url_api' => $request->api,
                'active' => $request->active,
                'icon' => $request->icon,
            ]);
            return back()->with('flash', 'Datos guardados correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo guardar la información, contacte a soporte');
        }
    }

    public function editSocial(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('social_media')
                ->where('id', $request->id)
                ->update([
                    'name_social' => $request->name,
                    'url_api' => $request->api,
                    'icon' => $request->icon,
                    'active' => $request->active,
                ]);
            return back()->with('flash', 'Información actualizada correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar');
        }
    }

    public function deleteSocial(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('social_media')->where('id', $request->id)->delete();
            return back()->with('flash', 'Borrado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo borrar');
        }
    }

    public function phoneExtensions()
    {
        $arrPhones = DB::select('SELECT id, name_location, number_extension, pivot FROM phones_extensions');
        return view('moduleConfig.phones', ['phones' => $arrPhones]);
    }

    public function createPhone(Request $request)
    {
        if(!empty($request->name)){
            Phones::create([
                'name_location' => $request->name,
                'number_extension' => $request->number_extension,
                'pivot' => $request->pivot,
            ]);
            return back()->with('flash', 'Datos guardados correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo guardar la información, contacte a soporte');
        }
    }

    public function editPhone(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('phones_extensions')
                ->where('id', $request->id)
                ->update([
                    'name_location' => $request->name,
                    'number_extension' => $request->number_extension,
                    'pivot' => $request->pivot,
                ]);
            return back()->with('flash', 'Información actualizada correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar');
        }
    }

    public function deletePhone(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('phones_extensions')->where('id', $request->id)->delete();
            return back()->with('flash', 'Borrado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo borrar');
        }
    }

    public function currencies()
    {
        $arrCurrencies = DB::select('SELECT id, name_currency, icon, fee, active FROM currencies');
        return view('moduleConfig.currencies', ['currencies' => $arrCurrencies]);
    }

    public function createCurrencies(Request $request)
    {
        if(!empty($request->name)){
            Currencies::create([
                'name_currency' => $request->name,
                'icon' => $request->icon,
                'fee' => $request->fee,
                'active' => $request->active,
            ]);
            return back()->with('flash', 'Datos guardados correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo guardar la información, contacte a soporte');
        }
    }

    public function editCurrencies(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('currencies')
                ->where('id', $request->id)
                ->update([
                    'name_currency' => $request->name,
                    'icon' => $request->icon,
                    'fee' => $request->fee,
                    'active' => $request->active,
                ]);
            return back()->with('flash', 'Información actualizada correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar');
        }
    }
    public function deleteCurrencies(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('currencies')->where('id', $request->id)->delete();
            return back()->with('flash', 'Borrado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo borrar');
        }
    }
}
