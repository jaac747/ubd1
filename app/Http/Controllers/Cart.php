<?php

namespace App\Http\Controllers;

use App\Clients;
use App\Estimates;
use App\EstimatesDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Keygen\Keygen;

class Cart extends Controller
{
    public function init()
    {
        $arrProducts = DB::select('SELECT 
                                    SPD.id_product,
                                    SPC.id AS "id_category",
                                    SPD.name_product_detail,
                                    SPD.type_product_detail,
                                    SPD.price_unity,
                                    SPD.price_weight,
                                    SPD.active,
                                    SPD.offer,
                                    SPD.time_limit,
                                    SPD.quantity_exist_unity,
                                    SPD.quantity_exist_weight
                                    
                                    FROM sales_products_detail SPD
                                        INNER JOIN sales_products SP
                                            ON SPD.id_product = SP.id
                                        INNER JOIN SALE_CATEGORIES SPC
                                            ON SP.id_category = SPC.id
                                    WHERE 
                                        SPC.active = \'Y\' 
                                        AND SP.active = \'Y\'
                                    AND SPD.active = \'Y\'');
        foreach($arrProducts AS $key => $value) {

        }
        return view('moduleSales.cart', array('products' => $arrProducts));
    }

    public function shop(Request $request)
    {
        if(!empty($request->product)) {
            $intClient = "";
            if(!empty($request->code) || !empty($request->mail)) {
                $response = DB::select("SELECT id FROM clients WHERE code = '{$request->code}' OR mail = '{$request->mail}'");
                if(count($response) > 0) {
                    $intClient = $response[0]->id;
                    if(empty($intClient)) {
                        $intClient = $this->generateClient($request);
                    }
                }
            }
            else {
                $intClient = $this->generateClient($request);
            }

            $arrEstimates = DB::select('SELECT count(*) AS estimate FROM estimates');
            $intEstimates = 1;
            if(count($arrEstimates) > 0) {
                if(!empty($arrEstimates[0]->estimate) && ($arrEstimates[0]->estimate > 0)) {
                    $intEstimates = $arrEstimates[0]->estimate;
                }
            }
            $intEstimates = str_pad($intEstimates, 10, '0', STR_PAD_LEFT);
            $intTotal = 0;


            foreach ($request->product AS $key => $value) {
                $intTotal += ($request->price[$key] * 1) * ($request->quantity[$key]);
            }

            $estimate = Estimates::create([
                'id_user' => 0,
                'correlative' => $intEstimates,
                'objEstimate' => '{}',
                'total_estimate' => ($intTotal),
                'client_id' => $intClient,
                'client_name' => $request->name,
                'client_email' => $request->mail,
                'client_phone' => $request->phone,
                'client_address' => $request->address,
<<<<<<< HEAD
                
=======
                'status' => 'Solicitado',
>>>>>>> c68e0e7348be7a0b998ef653149d3448af5e2f5f
            ]);
            $idEstimate = $estimate->id;

            foreach ($request->product AS $key => $value) {
                EstimatesDetail::create([
                    'id_estimate' => $idEstimate,
                    'id_product' => $value,
                    'quantity' => $request->quantity[$key],
                    'category' => $request->category[$key],
                    'price' => $request->price[$key],
                    'total_individual_product_estimate' => 0,
                ]);
            }
            return redirect()->to('tarjeta');;
        }
    }

    public function generateClient($infoRequest)
    {
        $numb = Keygen::numeric(4)->generate();
        $dataClient = Clients::create([
            'name_client' => $infoRequest->name,
            'phone' => $infoRequest->phone,
            'mail' => $infoRequest->mail,
            'address' => $infoRequest->address,
            'code' => substr($infoRequest->name[0],0, 1).$numb,
            'active' => 'Y',
        ]);

        return $dataClient->id;
    }

    public function statusUpdate(Request $request)
    {
        if(!empty($request->estimate)) {
            DB::table('estimates')
                ->where('id', $request->estimate)
                ->update([
                    'status' => $request->status,
                ]);

            if($request->status == 'entregado') {
                $arrEstimate = DB::select("SELECT id_product, quantity FROM estimates_products_detail WHERE id_estimate = '{$request->estimate}'");
                if(is_array($arrEstimate) && count($arrEstimate) > 0) {
                    foreach ($arrEstimate AS $k => $v) {
                        DB::update("UPDATE sales_products_detail SET quantity_exist_unity = quantity_exist_unity - {$v->quantity} , quantity_exist_weight = quantity_exist_weight - {$v->quantity} WHERE id = {$v->id_product}");
                    }
                }
            }
            return back()->with('flash', 'Información actualizada correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar la categoría');
        }
    }
}
