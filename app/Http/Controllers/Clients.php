<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Clients extends Controller
{
    public function init()
    {
        return view('moduleClients.clients', ['clients' => array()]);
    }
}
