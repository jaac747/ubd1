<?php

namespace App\Http\Controllers;

use App\Estimates;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\core_menu_set_session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $a = new core_menu_set_session();
        $a->setMenu();
        $arrEstimates = DB::select('SELECT * FROM estimates');
        $arrDetailTMP = DB::select('SELECT 
                                            EPD.id_estimate,
                                            EPD.id_product,
                                            EPD.quantity,
                                            SPD.name_product_detail,
                                            E.status
                                            FROM estimates_products_detail EPD 
                                            INNER JOIN sales_products_detail SPD 
                                            ON EPD.id_product = SPD.id
                                            LEFT JOIN estimates E
                                            ON EPD.id_estimate = E.id
                                            WHERE E.status != \'entregado\'');
        $arrDetail = array();
        if(!empty($arrDetailTMP)) {
            foreach ($arrDetailTMP AS $key => $value) {
                $value->quantity = $value->quantity * 1;
                if(empty($arrDetail[$value->id_estimate])){
                    $arrDetail[$value->id_estimate] = array();
                    $arrDetail[$value->id_estimate]["id_estimate"] = $value->id_estimate;
                    $str = "{$value->quantity} {$value->name_product_detail} , ";
                    $arrDetail[$value->id_estimate]["detail"] = $str;
                }
                else {
                    $str = "{$value->quantity} {$value->name_product_detail} , ";
                    $arrDetail[$value->id_estimate]["detail"] .= $str;
                }
            }
            /*dd($arrDetail);*/
        }

        return view('home', array('arrEstimates' => $arrEstimates, 'arrDetail' => $arrDetail));
    }

    public function myAccount()
    {
        $arrDataUser = Auth::user();
        return view('myAccount', ['arrDataUser' => $arrDataUser]);
    }

    public function updateMyAccount(Request $request)
    {
        if(!empty($request->user)){;
            if(!empty($request->password)) {
                DB::table('users')
                    ->where('id', $request->user)
                    ->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        'password' => Hash::make($request->password),
                        'un_password' => $request->password,
                    ]);
            }
            else {
                DB::table('users')
                    ->where('id', $request->id)
                    ->update([
                        'name' => $request->name,
                        'email' => $request->email,
                    ]);
            }

            return back()->with('flash', 'Información actualizada correctamente');
        }
        else {
            return back()->with('flash', 'No está enviando que usuario quiere actualizar');
        }
    }
}
