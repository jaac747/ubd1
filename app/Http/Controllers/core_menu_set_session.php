<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\core;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use function Psy\debug;

class core_menu_set_session extends Controller
{

    public function setMenu ()
    {

        if(!empty( session()->get('menu') )){
            return session()->get('menu');
        }
        else {
            $arrCore = new core();
            $arrDataUser = Auth::user();
            $intUserID = $arrDataUser->id;
            $boolAdmin = ( $arrDataUser->id_user_profile == 1 ) ? true : false;
            $arrWindows = $arrCore->getOptionsMenu($intUserID, $boolAdmin);
            $arrReturn = [];

            foreach ($arrWindows AS $key => $value) {
                if(empty($arrReturn[$value->id_module])) {
                    $arrReturn[$value->id_module] = [];
                    $arrReturn[$value->id_module]["detail"] = [];
                    $arrReturn[$value->id_module]["name"] = "";
                }

                $arrReturn[$value->id_module]["name"] = $value->name_module;
                $arrReturn[$value->id_module]["detail"][] = $value;
            }
            Session::push('menu', $arrReturn);
            return session()->get('menu');
        }
    }
}
