<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class CategoriesController extends Controller
{
    public function init()
    {
        $arrModules = DB::select('SELECT id, name_category, image_category, active, icon FROM SALE_CATEGORIES WHERE active = \'Y\'');
        $arrProducts = DB::select('SELECT P.id, name_product, id_category, description_product, image_product, type_product, currency, P.min_exist, C.icon,  P.active, PC.name_category
                                            FROM
                                                sales_products P
                                            INNER JOIN SALE_CATEGORIES PC 
                                            ON P.id_category = PC.id
                                            LEFT JOIN currencies C
                                            ON P.currency = C.id
                                            WHERE PC.active = \'Y\'');
        $arrCurrencies = DB::select('SELECT id, icon FROM currencies WHERE active = \'Y\'');

        return view('moduleSales.categories', ['categories' => $arrModules, 'products' => $arrProducts, 'currencies' => $arrCurrencies]);
    }

    public function createCategory(Request $request)
    {
        if(!empty($request->name)){
            $name = strtolower($request->name);
            $strQuickName = str_replace(' ', '_', $name);
            $strDirectory = public_path().'/var/images';
            $filename = "";
            if($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move($strDirectory, $filename);
            }
            Categories::create([
                'name_category' => utf8_encode($request->name),
                'quick_name_category' => utf8_encode($strQuickName),
                'active' => $request->active,
                'image_category' => $filename,
                'icon' => $request->icon,
            ]);
            return back()->with('flash', 'Categoría creada correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo crear la categoría, contacte a soporte');
        }
    }

    public function editCategory(Request $request)
    {
        if(!empty($request->name)) {
            $name = strtolower($request->name);
            $strQuickName = str_replace(' ', '_', $name);
            $strDirectory = public_path().'/var/images';
            $filename = "";
            if($request->hasfile('image')) {
                $this->deleteImageCategoryExist($request->category);
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move($strDirectory, $filename);
            }
            if(empty($filename)) {
                $image = DB::select("SELECT image_category FROM SALE_CATEGORIES WHERE id = '{$request->category}'");
                $filename = $image[0]->image_category;
            }
            DB::table('SALE_CATEGORIES')
                ->where('id', $request->category)
                ->update([
                    'name_category' => utf8_encode($request->name),
                    'quick_name_category' => utf8_encode($strQuickName),
                    'active' => $request->active,
                    'image_category' => $filename,
                    'icon' => $request->icon,
                ]);
            return back()->with('flash', 'Información actualizada correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar la categoría');
        }
    }

    public function deleteImageCategoryExist($intCategory)
    {
        if(!empty($intCategory)) {
            $image = DB::select("SELECT image_category FROM SALE_CATEGORIES WHERE id = '{$intCategory}'");
            if(!empty($image[0]->image_category)) {
                if(file_exists(public_path()."/var/images/{$image[0]->image_category}")) {
                    unlink(public_path()."/var/images/{$image[0]->image_category}");
                }
            }
        }
    }

    public function deleteImageProductExist($intProduct)
    {
        if(!empty($intProduct)) {
            $image = DB::select("SELECT image_product FROM sales_products WHERE id = '{$intProduct}'");
            if(!empty($image[0]->image_product)) {
                if(file_exists(public_path()."/var/images/{$image[0]->image_product}")) {
                    unlink(public_path()."/var/images/{$image[0]->image_product}");
                }
            }
        }
    }

    public function deleteCategory(Request $request)
    {
        if(!empty($request->category)) {
            DB::table('SALE_CATEGORIES')->where('id', $request->category)->delete();
            /*DB::table('SALE_CATEGORIES')
                ->where('id', $request->category)
                ->update([
                    'active' => 'N'
                ]);*/
            return back()->with('flash', 'Categoría borrada correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo eliminar la categoría');
        }
    }

    public function createProduct(Request $request)
    {
        if(!empty($request->name)){
            $strDirectory = public_path().'/var/images';
            $filename = "";
            if($request->hasfile('image')) {
                $this->deleteImageCategoryExist($request->category);
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move($strDirectory, $filename);
            }
            Products::create([
                'name_product' => utf8_encode($request->name),
                'description_product' => utf8_encode($request->description),
                'active' => $request->active,
                'image_product' => $filename,
                'id_category' => $request->category,
                'currency' => $request->currency,
                'type_product' => $request->type_product,
                'min_exist' => $request->min_exist,
            ]);
            return back()->with('flash', 'Producto creado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo crear el producto, contacte a soporte');
        }
    }

    public function editProduct(Request $request)
    {
        if(!empty($request->product)) {
            $strDirectory = public_path().'/var/images';
            $filename = "";
            if($request->hasfile('image')) {
                $this->deleteImageProductExist($request->name);
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move($strDirectory, $filename);
            }
            if(empty($filename)) {
                $image = DB::select("SELECT image_product FROM sales_products WHERE id = {$request->product}");
                $filename = $image[0]->image_product;
            }
            DB::table('sales_products')->where('id', $request->product)->update([
                'name_product' => utf8_encode($request->name),
                'description_product' => utf8_encode($request->description),
                'active' => $request->active,
                'image_product' => $filename,
                'id_category' => $request->category,
                'currency' => $request->currency,
                'type_product' => $request->type_product,
                'min_exist' => $request->min_exist,
            ]);
            return back()->with('flash', 'Producto editado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo editar el producto');
        }
    }

    public function deleteProduct(Request $request)
    {
        if(!empty($request->product)) {
            DB::table('sales_products')->where('id', $request->product)->delete();
            return back()->with('flash', 'Producto borrado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo eliminar el producto');
        }
    }
}
