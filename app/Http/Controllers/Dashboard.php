<?php

namespace App\Http\Controllers;

use App\Estimates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Dashboard extends Controller
{
    public function init(Request $request)
    {
        /*$arrDone = DB::select("
            SELECT
                SUM(total_estimate) AS total,
                CONCAT(MONTH(created_at), '/', YEAR(created_at)) AS `date`
            FROM estimates
            WHERE 1
            AND `status` = 'entregado'
            GROUP BY MONTH(created_at)
            LIMIT 6");*/
        $arrDone = DB::table('estimates')
            ->selectRaw("SUM(total_estimate) AS total, MONTH(created_at) AS month")
            ->where('status', '=', 'entregado')
            ->orderBy('created_at')
            ->groupBy(DB::raw("MONTH(created_at)"))
            ->get();
        dd($arrDone);

        $arrNone = array();
        /*$arrNone = DB::select("
            SELECT
                SUM(total_estimate) AS total,
                CONCAT(MONTH(created_at), '/', YEAR(created_at)) AS date
            FROM estimates
            WHERE 1
            AND `status` = 'Solicitado'
            GROUP BY MONTH(created_at)
            LIMIT 6");*/

        $from = $request->get('from');
        $to = $request->get('to');


        /*$arrEstimates = Estimates::whereBetween('created_at', [, $request->get('to')])->get();*/
        $arrEstimates = array();
        return view('moduleSales.dashboard', array('done' => $arrDone, 'none' => $arrNone,'dashboard' => $arrEstimates));
    }
}
