<?php

namespace App\Http\Controllers;

use App\ProductsDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class SalesConfigProducts extends Controller
{
    //

    public function init()
    {
        $arrProducts = DB::select('SELECT 
                                                PD.id,
                                                PD.name_product_detail AS "name",
                                                PD.type_product_detail,
                                                PD.id_product,
                                                PD.price_unity,
                                                PD.price_weight,
                                                PD.price_before_update,
                                                PD.active,
                                                PD.quantity_exist_unity,
                                                PD.quantity_exist_weight,
                                                PD.quantity_reservation_unity,
                                                PD.quantity_reservation_weight,
                                                PD.offer,
                                                PD.image,
                                                PC.name_category,
                                                P.min_exist,
                                                P.id_provider,
                                                PV.phone,
                                                PV.id AS "id_provider",
                                                PV.link_share
                                            FROM sales_products_detail PD
                                            INNER JOIN sales_products P
                                            ON PD.id_product = P.id
                                            INNER JOIN SALE_CATEGORIES PC
                                            ON P.id_category = PC.id
                                            LEFT JOIN providers PV
                                            ON P.id_provider = PV.id
                                            WHERE P.active = \'Y\' AND PC.active = \'Y\'');
        if(!empty($arrProducts)){
            foreach ($arrProducts AS $key => $value) {
                $value->boolRed = false;
                if($value->type_product_detail == "unity"){
                    $value->price = $value->price_unity;
                    $value->quantity = $value->quantity_exist_unity;
                }
                elseif ($value->type_product_detail == "weight") {
                    $value->price = $value->price_weight;
                    $value->quantity = $value->quantity_exist_weight;
                }

                if($value->min_exist > $value->quantity) {
                    $value->boolRed = true;
                }
            }
        }
        
        $arrProviders = DB::select('SELECT id, name_provider, phone, link_share FROM providers ');
        $arrProduct = DB::select('SELECT id, name_product FROM sales_products WHERE active =\'Y\'');
        
        return view('moduleSales.configProducts', ['products' => $arrProducts,  'providers' => $arrProviders, 'prd' => $arrProduct]);
    }

    public function deleteProduct(Request $request)
    {
        if(!empty($request->id)) {
            DB::table('sales_products_detail')->where('id', $request->id)->delete();
            return back()->with('flash', 'Producto borrado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo eliminar el producto');
        }
    }

    public function createProduct(Request $request)
    {
        $strDirectory = public_path().'/var/images';
        $filename = "";
        if($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move($strDirectory, $filename);
        }
        //dd($request);

        if(!empty($request->name)){
            /*try {
                $a = DB::table('sales_products_detail')->insert([
                    'id_product' => $request->product,
                    'name_product_detail' => $request->name,
                    'type_product_detail' => $request->type_product,
                    'price_unity' => $request->fee,
                    'price_weight' => $request->fee,
                    'active' => $request->active,
                    'offer' => $request->offer,
                    'quantity_exist_unity' => $request->quantity,
                    'quantity_exist_weight' => $request->quantity,
                    'id_provider' => $request->provider,
                    'image' => $filename
                ]);
            }
            catch (\Exception $e) {
                dd($e);
            }*/

            ProductsDetail::create([
                'id_product' => $request->product,
                'name_product_detail' => $request->name,
                'type_product_detail' => $request->type_product,
                'price_unity' => $request->fee,
                'price_weight' => $request->fee,
                'active' => $request->active,
                'offer' => $request->offer,
                'time_limit' => '6',
                'quantity_exist_unity' => $request->quantity,
                'quantity_exist_weight' => $request->quantity,
               // 'id_provider' => $request->provider,
                'image' => $filename
            ]);
            return back()->with('flash', 'Producto creado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo crear el producto, contacte a soporte');
        }
    }

    public function editProduct(Request $request)
    {
        if(!empty($request->id)) {
            $strDirectory = public_path().'/var/images';
            $filename = "";
            if($request->hasfile('image')) {
                $this->deleteImageProduct($request->id);
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $file->move($strDirectory, $filename);
            }
            DB::table('sales_products_detail')->where('id', $request->id)->update([
                'name_product_detail' => $request->name,
                'type_product_detail' => $request->type_product,
                'price_unity' => $request->fee,
                'price_weight' => $request->fee,
                'active' => $request->active,
                'quantity_exist_unity' => $request->quantity,
                'quantity_exist_weight' => $request->quantity,
                //'id_provider' => $request->provider,
                'id_product' => $request->product,
                'offer' => $request->offer,
                'date_of_expiry' => date('Y-m-d'),
                'image' => $filename,
            ]);
            return back()->with('flash', 'Producto editado correctamente');
        }
        else {
            return back()->with('flash', 'No se pudo eliminar el producto');
        }
    }

    public function deleteImageProduct($intProduct)
    {
        $image = DB::select("SELECT image FROM sales_products_detail WHERE id = '{$intProduct}'");
        if(!empty($image[0]->image)) {
            if(file_exists(public_path()."/var/images/{$image[0]->image}")) {
                unlink(public_path()."/var/images/{$image[0]->image}");
            }
        }
    }
}
