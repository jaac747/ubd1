<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currencies extends Model
{
    public $sequence  = 'SEQUENCE_CURRENCIES';
    protected $table = 'currencies';
    protected $guarded = ['id'];
}
