<?php

namespace App\Imports;

use App\Contact;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportContacts implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        

        return new Contact([
            //
            'id_product'     => $row[0],
            'name_product_detail'    => $row[1], 
            'type_product_detail'    => $row[2],
            'price_unity'    => $row[3],
            'price_weight'    => $row[4],
            //'price_before_update'    => $row[5],
            'active'    => $row[5],
            'description'    => $row[6],
            'offer'    => $row[7],
            'time_limit'    => $row[8],
            //'date_of_expiry'    => $row[10],
            'quantity_exist_unity'    => $row[9],
            //'quantity_reservation_unity'    => $row[12],
            'quantity_exist_weight'    => $row[10],
            //'quantity_reservation_weight'    => $row[14],
            //'code_inventory'    => $row[15],
            'created_at'    => $row[11],
            'updated_at'    => $row[12],
            //'icon'    => $row[18],
            'image'    => $row[13],
            'id_provider' => $row[14],


        ]);
    }
}