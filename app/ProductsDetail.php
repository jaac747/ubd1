<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsDetail extends Model
{
    public $sequence  = 'SEQUENCE_PRODUCT_DETAIL';
    protected $table = 'sales_products_detail';
    protected $guarded = ['id'];
}
