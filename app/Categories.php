<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{   
    public $sequence  = 'SEQUENCE_CATEGORIES';
    protected $table = 'SALE_CATEGORIES';
    protected $guarded = ['id'];
}
