<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\DB;

Route::get('/', function () {
    $arrTMPProducts = DB::select('SELECT 
                                    SPD.id_product,
                                    SPC.id AS "id_category",
                                    SPD.name_product_detail,
                                    SPD.type_product_detail,
                                    SPD.price_unity,
                                    SPD.price_weight,
                                    SPD.active,
                                    SPD.offer,
                                    SPD.time_limit,
                                    SPD.quantity_exist_unity,
                                    SPD.quantity_exist_weight,
                                    SPC.name_category,
                                    SPC.icon AS "icon_category",
                                    SP.name_product,
                                    SPD.id,
                                    SPD.image
                                    FROM sales_products_detail SPD
                                        INNER JOIN sales_products SP
                                            ON SPD.id_product = SP.id
                                        INNER JOIN SALE_CATEGORIES SPC
                                            ON SP.id_category = SPC.id
                                            INNER JOIN providers P
                                            ON SPD.id_provider = P.id
                                    WHERE 
                                        SPC.active = \'Y\' 
                                        AND SP.active = \'Y\'
                                    AND SPD.active = \'Y\'
                                       AND P.datei <= sysdate
                                        AND P.datef >= sysdate');
    $arrProducts = array();
    foreach($arrTMPProducts AS $key => $value) {
        if(empty($arrProducts[$value->id_category])) {
            $arrProducts[$value->id_category] = array();
            $arrProducts[$value->id_category]["category"] = $value->name_category;
            $arrProducts[$value->id_category]["icon"] = $value->icon_category;
            $arrProducts[$value->id_category]["id_category"] = $value->id_category;
            $arrProducts[$value->id_category]["detail"] = array();
        }
        if(empty($arrProducts[$value->id_category]["detail"][$value->id_product])) {
            $arrProducts[$value->id_category]["detail"][$value->id_product] = array();
            $arrProducts[$value->id_category]["detail"][$value->id_product]["product"] = $value->name_product;
            $arrProducts[$value->id_category]["detail"][$value->id_product]["detail"] = array();
        }
        $arrProducts[$value->id_category]["detail"][$value->id_product]["detail"][] = $value;
    }
    $arrCategories = array();
    return view('welcome', array('products' => $arrProducts, 'categories' => $arrCategories));
});

Route::get('importar-productos', 'ImportExcel\ImportExcelController@index');
Route::post('importar-productos', 'ImportExcel\ImportExcelController@import');

Route::get('/reg', function () {
    return view('loginuser');
});

Route::post ('registrar','LoginUsuarioController@RegistroCliente');

Route::post ('verificar','LoginUsuarioController@LoginUsuario');

Route::get('/tarjeta', function () {
        return view('tarjeta');
});


Route::get('/admin', function () {
    return view('admin');
});

Route::get('/cart', 'Cart@init');
Route::get('/dashboard', 'Dashboard@init')->name('dashboard');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/my-account', 'HomeController@myAccount')->name('myAccount');
Route::post('/my-account/update', 'HomeController@updateMyAccount')->name('user.updateUser');


/* ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== */
Route::get('/configuration', 'ConfigWebMasterController@init');

Route::post('/configuration/createModule', 'ConfigWebMasterController@createModule')->name('configuration.createModule');
Route::post('/configuration/editModule', 'ConfigWebMasterController@editModule')->name('configuration.editModule');
Route::post('/configuration/deleteModule', 'ConfigWebMasterController@deleteModule')->name('configuration.deleteModule');

Route::post('/configuration/createDetailModule', 'ConfigWebMasterController@createDetailModule')->name('configuration.createDetailModule');
Route::post('/configuration/editDetailModule', 'ConfigWebMasterController@editDetailModule')->name('configuration.editDetailModule');
Route::post('/configuration/deleteDetailModule', 'ConfigWebMasterController@deleteDetailModule')->name('configuration.deleteDetailModule');

/* ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== */
Route::get('/currencies', 'ConfigWebMasterController@currencies');

Route::post('/currencies/create', 'ConfigWebMasterController@createCurrencies')->name('configuration.createCurrencies');
Route::post('/currencies/edit', 'ConfigWebMasterController@editCurrencies')->name('configuration.editCurrencies');
Route::post('/currencies/delete', 'ConfigWebMasterController@deleteCurrencies')->name('configuration.deleteCurrencies');


/* ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== */
Route::get('/categories', 'CategoriesController@init');

Route::post('/categories/create', 'CategoriesController@createCategory')->name('category.createCategory');
Route::post('/categories/edit', 'CategoriesController@editCategory')->name('category.editCategory');
Route::post('/categories/delete', 'CategoriesController@deleteCategory')->name('category.deleteCategory');
Route::post('/categories/create-product', 'CategoriesController@createProduct')->name('category.createProduct');
Route::post('/categories/edit-product', 'CategoriesController@editProduct')->name('category.editProduct');
Route::post('/categories/delete-product', 'CategoriesController@deleteProduct')->name('category.deleteProduct');


/* ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== */
Route::get('/admin-products', 'SalesConfigProducts@init');
Route::post('/admin-products/getProduct', 'SalesConfigProducts@getProduct')->name('product.getProduct');
Route::post('/admin-products/deleteProduct', 'SalesConfigProducts@deleteProduct')->name('product.deleteProduct');
Route::post('/admin-products/editProduct', 'SalesConfigProducts@editProduct')->name('product.editProduct');
Route::post('/admin-products/createProduct', 'SalesConfigProducts@createProduct')->name('product.createProduct');


/* ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== */
Route::get('/providers', 'Providers@init');
Route::post('/providers/create', 'Providers@create')->name('providers.create');
Route::post('/providers/delete', 'Providers@delete')->name('providers.delete');
Route::post('/providers/edit', 'Providers@edit')->name('providers.edit');


/* ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== */
Route::post('/shop', 'Cart@shop')->name('shop.cart');





Route::post('/shop/edit', 'Cart@statusUpdate')->name('cart.editStatus');


/* ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== ========== */
Route::get('/clients', 'Clients@init');

Route::get('/tarjeta', function () {
    return view('tarjeta');
});
