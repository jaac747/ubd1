<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_client');
            $table->string('image')->nullable(true);
            $table->integer('phone')->nullable(true);
            $table->string('mail')->nullable(true);
            $table->string('address')->nullable(true);
            $table->string('code')->unique();
            $table->enum('active', array('Y', 'N'))->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
