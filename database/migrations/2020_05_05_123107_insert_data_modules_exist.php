<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataModulesExist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('core_modules')->insert([
            'name_module' => 'Productos',
            'quick_name_module' => 'products',
            'only_webmaster' => 'N',
        ]);

        DB::table('core_modules_detail')->insert([
            'id_module' => '2',
            'name_window' => 'Categorias productos',
            'quick_name_window' => '--',
            'route' => 'categories',
            'description_name_window' => '--',
        ]);

        DB::table('core_modules_detail')->insert([
            'id_module' => '2',
            'name_window' => 'Administracion productos',
            'quick_name_window' => '--',
            'route' => 'admin-products',
            'description_name_window' => '--',
        ]);

        DB::table('core_modules_detail')->insert([
            'id_module' => '2',
            'name_window' => 'Redes Sociales',
            'quick_name_window' => '--',
            'route' => 'social-media',
            'description_name_window' => '--',
        ]);

        DB::table('core_modules_detail')->insert([
            'id_module' => '2',
            'name_window' => 'Extensiones telefonicas',
            'quick_name_window' => '--',
            'route' => 'phone-extensions',
            'description_name_window' => '--',
        ]);

        DB::table('core_modules_detail')->insert([
            'id_module' => '2',
            'name_window' => 'Monedas',
            'quick_name_window' => '--',
            'route' => 'currencies',
            'description_name_window' => '--',
        ]);

        DB::table('core_modules_detail')->insert([
            'id_module' => '2',
            'name_window' => 'Proveedores',
            'quick_name_window' => '--',
            'route' => 'providers',
            'description_name_window' => '--',
        ]);*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
