<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SalesProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_products_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_category')->nullable(true);
            $table->string('quick_name_category')->nullable(true);
            $table->string('image_category')->nullable(true);
            $table->enum('active', array('Y', 'N'))->default('Y');
            $table->timestamps();
        });

        Schema::create('sales_products', function (Blueprint $table) {
            /*
             * type_product = este union es por algún código y se arman arreglos
             * column_union = es el nombre por el cuál vamos a unir a nuestro producto
             * id_provider = id del proveedor para que se pueda mostrar en un pop-up la información y contactar
             * */
            $table->bigIncrements('id');
            $table->string('name_product')->nullable(true);
            $table->longText('description_product')->nullable(true);
            $table->enum('active', array('Y', 'N'))->default('Y');
            $table->string('image_product')->nullable(true);
            $table->integer('id_category')->nullable(true);
            $table->integer('id_father_product')->nullable(true);
            $table->enum('type_product',['simple', 'union'])->default('simple')->nullable(true);
            $table->string('column_union')->nullable(true);
            $table->string('currency')->nullable(true);
            $table->integer('min_exist')->nullable(true);
            $table->integer('id_provider')->nullable(true);
            $table->timestamps();
        });

        Schema::create('sales_products_detail', function (Blueprint $table) {
            /*
             * date_of_expiry = fecha de caducidad
             * quantity_reservation = reserva que se prevee un almacén, no una venta constante
             * type_product_detail = define que tipo de producto es, si se vende por unidad o por peso
             * price_before_update = precio al que se compró antes de la actualización, por seguridad si se da a lo mismo o se llegara a cambiar por los proveedores
             * */
            $table->bigIncrements('id');
            $table->integer('id_product')->nullable(true);
            $table->string('name_product_detail')->nullable(true);
            $table->enum('type_product_detail', array('unity', 'weight'))->default('unity');
            $table->decimal('price_unity', 4)->nullable(true);
            $table->decimal('price_weight', 4)->nullable(true);
            $table->integer('price_before_update')->nullable(true);
            $table->enum('active', array('Y', 'N'))->default('Y');
            $table->string('description')->nullable(true);
            $table->string('offer')->nullable(true);
            $table->enum('time_limit', array('3', '6', '9', '12', '18', '24'))->nullable(true);
            $table->date('date_of_expiry')->nullable(true);
            $table->integer('quantity_exist_unity')->nullable(true);
            $table->integer('quantity_reservation_unity')->nullable(true);
            $table->integer('quantity_exist_weight')->nullable(true);
            $table->integer('quantity_reservation_weight')->nullable(true);

            $table->string('code_inventory')->nullable(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_products_categories');
        Schema::dropIfExists('sales_products');
        Schema::dropIfExists('sales_products_detail');
    }
}
