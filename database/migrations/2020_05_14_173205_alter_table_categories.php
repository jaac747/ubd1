<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_products_categories', function (Blueprint $table) {
            $table->string('icon')->nullable(true);
        });

        Schema::table('sales_products', function (Blueprint $table) {
            $table->string('icon')->nullable(true);
        });

        Schema::table('sales_products_detail', function (Blueprint $table) {
            $table->string('icon')->nullable(true);
            $table->string('image')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_products_categories', function (Blueprint $table) {
            $table->dropColumn('icon');
        });
        Schema::table('sales_products', function (Blueprint $table) {
            $table->dropColumn('icon');
        });
        Schema::table('sales_products_detail', function (Blueprint $table) {
            $table->dropColumn('icon');
            $table->string('image');
        });
    }
}
