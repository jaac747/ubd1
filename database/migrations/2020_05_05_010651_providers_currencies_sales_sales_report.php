<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProvidersCurrenciesSalesSalesReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_currency')->unique();
            $table->string('icon')->unique();
            $table->decimal('fee', 4);
            $table->string('url_api')->nullable(true);
            $table->enum('active', array('Y', 'N'))->default('Y');
            $table->timestamps();
        });

        Schema::create('phones_extensions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_location');
            $table->integer('number_extension');
            $table->enum('pivot', array('Y', 'N'))->default('N');
            $table->timestamps();
        });

        Schema::create('social_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_social');
            $table->string('url_api');
            $table->enum('active', array('Y', 'N'))->default('N');
            $table->string('icon');
            $table->timestamps();
        });

        Schema::create('providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_provider');
            $table->string('address')->unique();
            $table->string('phone')->nullable(true);
            $table->string('phone_two')->nullable(true);
            $table->enum('link_share', array('Y', 'N'))->default('N');
            $table->enum('link_share_two', array('Y', 'N'))->default('N');
            $table->string('email')->nullable(true);
            $table->timestamps();
        });

        Schema::create('estimates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_user');
            $table->integer('correlative')->nullable(true);
            $table->longText('objEstimate')->nullable(true);
            $table->decimal('total_estimate', 4)->nullable(true);
            $table->string('client_id')->nullable(true);
            $table->string('client_name')->nullable(true);
            $table->string('client_email')->nullable(true);
            $table->string('client_address')->nullable(true);
            $table->string('client_phone')->nullable(true);
            $table->enum('status', array('solicitado', 'enviado', 'entregado', 'pagado'))->default('solicitado');
            $table->timestamps();
        });

        Schema::create('estimates_products_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_estimate');
            $table->integer('id_product');
            $table->decimal('quantity', 4)->nullable(true);
            $table->integer('category')->nullable(true);
            $table->decimal('price', 4)->nullable(true);
            $table->decimal('total_individual_product_estimate', 4)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('phones_extensions');
        Schema::dropIfExists('social_media');
        Schema::dropIfExists('providers');
        Schema::dropIfExists('estimates');
        Schema::dropIfExists('estimates_products_detail');
    }
}
