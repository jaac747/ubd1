<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable(true);
            $table->string('password');
            $table->string('un_password')->nullable(true);
            $table->integer('id_user_profile')->nullable(true);

            $table->integer('id_role_access')->nullable(true);
            $table->integer('id_father_role')->nullable(true);
            $table->integer('id_child_role')->nullable(true);

            $table->integer('id_father_school')->nullable(true);
            $table->integer('id_child_school')->nullable(true);
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'name' => 'Webmaster',
            'email' => 'najcheu@gmail.com',
            'password' => Hash::make('hola'),
            'un_password' => '',
            'id_user_profile' => '1',
            'id_role_access' => '1',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
