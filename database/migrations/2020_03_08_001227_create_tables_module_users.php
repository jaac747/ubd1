<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesModuleUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_profile')->unique();
            $table->longText('description_profile')->nullable(true);
            $table->timestamps();
        });

        Schema::create('users_profiles_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_user_profile');
            $table->integer('id_modules_detail');
            $table->timestamps();
        });

        Schema::create('core_modules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_module')->unique();
            $table->string('quick_name_module')->unique();
            $table->enum('only_webmaster', ['Y', 'N'])->default('N');
            $table->timestamps();
        });

        Schema::create('core_modules_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_module');
            $table->string('name_window');
            $table->string('quick_name_window');
            $table->string('route')->unique();
            $table->longText('description_name_window');
            $table->timestamps();
        });

        DB::table('users_profiles')->insert([
            'name_profile' => 'Webmaster',
            'description_profile' => 'Webmaster',
        ]);

        DB::table('core_modules')->insert([
            'name_module' => 'Configuración',
            'quick_name_module' => 'configuracion',
            'only_webmaster' => 'Y',
        ]);

        DB::table('core_modules_detail')->insert([
            'id_module' => '1',
            'name_window' => 'Configuración de Ventanas',
            'quick_name_window' => 'configuracion_de_ventanas',
            'description_name_window' => 'Configuración solo para usuario webmaster',
            'route' => 'configuration',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_profiles');

        Schema::dropIfExists('users_profiles_detail');

        Schema::dropIfExists('core_modules');

        Schema::dropIfExists('core_modules_detail');
    }
}
