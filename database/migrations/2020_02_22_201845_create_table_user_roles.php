<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_role');
            $table->string('quick_name_role');
            $table->integer('id_branch')->nullable(true);
            $table->string('color_role')->nullable(true);
            $table->integer('id_child_role')->nullable(true);
            $table->timestamps();
        });

        DB::table('users_roles')->insert([
            'id' => '1',
            'name_role' => 'Administrador',
            'quick_name_role' => 'admin',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_roles');
    }
}
